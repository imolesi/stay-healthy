var conn = new Mongo();
var db = conn.getDB('stayhealthy');

db.createCollection('achievements', function(err, collection) {});
db.createCollection('articles', function(err, collection) {});
db.createCollection('clients', function(err, collection) {});
db.createCollection('courses', function(err, collection) {});
db.createCollection('diets', function(err, collection) {});
db.createCollection('experts', function(err, collection) {});
db.createCollection('objectives', function(err, collection) {});
db.createCollection('tags', function(err, collection) {});
db.createCollection('users', function(err, collection) {});

try {
  db.achievements.deleteMany( { } );
  db.articles.deleteMany( { } );
  db.clients.deleteMany( { } );
  db.courses.deleteMany( { } );
  db.diets.deleteMany( { } );
  db.experts.deleteMany( { } );
  db.objectives.deleteMany( { } );
  db.tags.deleteMany( { } );
  db.users.deleteMany( { } );
} catch (e) {
  print(e);
}
