const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DaysEnum = Object.freeze({"monday":1, "tuesday":2, "wednesday":3, "thursday":4, "friday":5, "saturday":6, "sunday":0});

let Course = new Schema({
  course_name: {
    type: String,
    required: true
  },
  course_description: {
    type: String,
    required: true
  },
  course_trainer: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  course_avatar: {
    type: String
  },
  course_publicationDate: {
    type: Date,
    default: Date.now
  },
  course_startDate: {
    type: Date,
    required: true
  },
  course_endDate: {
    type: Date,
    required: true
  },
  course_days: {
    type: [Number],
    enum: Object.values(DaysEnum),
    default: Object.values(DaysEnum)
  },
  course_subscribers: [
    {
      subscriber: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
      }
    }
  ],
  course_objectives: [
    {
      objective: {
        type: Schema.Types.ObjectId,
        ref: 'Objective',
        required: true
      }
    }
  ]
});

module.exports = mongoose.model('Course', Course);
