const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Diet = new Schema({
  diet_name: {
    type: String,
    required: true,
    unique: true,
    sparse: true
  },
  diet_meals: [
    {
      meal_name: {
        type: String,
        required: true,
        unique: true,
        sparse: true
      },
      intake: [
        {
          nutrient: {
            type: String,
            required: true,
            unique: true,
            sparse: true
          },
          quantity: {
            type: Number,
            required: true
          }
        }
      ]
    }
  ],
  diet_description: {
    type: String
  },
  diet_author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});

module.exports = mongoose.model('Diet', Diet);
