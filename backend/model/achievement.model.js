const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Achievement = new Schema({
  achievement_name: {
    type: String,
    required: true
  },
  achievement_description: {
    type: String,
    required: true
  },
  achievement_image: {
    type: String
  }
});

module.exports = mongoose.model('Achievement', Achievement);
