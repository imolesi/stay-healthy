const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Client = new Schema({
  client_user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    unique: true
  },
  client_interests: [
    {
      tag: {
        type: Schema.Types.ObjectId,
        ref: 'Tag',
      }
    }
  ],
  client_objectives: [
    {
      objective: {
        type: Schema.Types.ObjectId,
        ref: 'Objective',
      }
    }
  ],
  client_weightProgress: [
    {
      weight: {
        type: Number,
        required: true,
        unique: true,
        sparse: true
      },
      date: {
        type: Date,
        default: Date.now,
        required: true,
        unique: true,
        sparse: true
      }
    }
  ],
  client_height: {
    type: Number,
    required: true
  },
  client_objectiveWeight: {
    type: Number,
    required: true
  },
  client_diet: {
    type: Schema.Types.ObjectId,
    ref: 'Diet',
    default: undefined
  },
  client_achievements: [
    {
      achievement: {
        type: Schema.Types.ObjectId,
        ref: 'Achievement',
      }
    }
  ],
});

module.exports = mongoose.model('Client', Client);
