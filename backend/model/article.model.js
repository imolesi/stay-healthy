const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Article = new Schema({
  article_title: {
    type: String,
    required: true
  },
  article_description: {
    type: String,
    required: true
  },
  article_author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  article_avatar: {
    type: String
  },
  article_publicationDate: {
    type: Date,
    default: Date.now
  },
  article_likes: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
      }
    }
  ],
  article_tags: [
    {
      tag: {
        type: Schema.Types.ObjectId,
        ref: 'Tag',
        required: true
      }
    }
  ],
  article_comments: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
      },
      text: {
        type: String,
        required: true
      },
      name: {
        type: String,
        required: true
      },
      avatar: {
        type: String
      },
      date: {
        type: Date,
        default: Date.now
      }
    }
  ]
});

module.exports = mongoose.model('Article', Article);
