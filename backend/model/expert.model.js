const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Expert = new Schema({
  expert_user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  expert_bio: {
    type: String
  },
  expert_knowledge: [
    {
      tag: {
        type: Schema.Types.ObjectId,
        ref: 'Tag',
        required: true
      }
    }
  ],
  expert_objectives: [
    {
      objective: {
        type: Schema.Types.ObjectId,
        ref: 'Objective',
        required: true
      }
    }
  ]
});

module.exports = mongoose.model('Expert', Expert);
