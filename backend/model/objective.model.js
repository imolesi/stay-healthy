const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Objective = new Schema({
  objective_name: {
    type: String,
    required: true,
    unique: true
  }
});

module.exports = mongoose.model('Objective', Objective);
