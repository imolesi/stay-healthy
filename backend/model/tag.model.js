const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Tag = new Schema({
  tag_name: {
    type: String,
    required: true,
    unique: true
  }
});

module.exports = mongoose.model('Tag', Tag);
