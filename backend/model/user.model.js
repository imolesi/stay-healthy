const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let KeysSchema = new Schema({
  p256dh: {
    type: String
  },
  auth: {
    type: String
  }
});

let SubscriptionSchema = new Schema({
  endpoint: {
    type: String,
    required: true
  },
  expirationTime: {
    type: Number
  },
  keys: {
    type: KeysSchema
  }
});

let User = new Schema({
  user_email: {
    type: String,
    required: true,
    unique: true
  },
  user_username: {
    type: String,
    required: true,
    unique: true
  },
  user_name: {
    type: String,
    required: true
  },
  user_lastname: {
    type: String,
    required: true
  },
  user_gender: {
    type: String,
    required: true
  },
  user_password: {
    type: String,
    required: true
  },
  user_birthDate: {
    type: Date,
    required: true
  },
  user_avatar: {
    type: String
  },
  user_registrationDate: {
    type: Date,
    default: Date.now
  },
  user_subscription: {
    type: SubscriptionSchema
  }
});

module.exports = mongoose.model('User', User);
