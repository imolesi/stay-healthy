const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const connectDB = require('./persistence/db');
const webpush = require('web-push');
const path = require('path');
const PORT = 4000;

const publicVapidKey =
  'BP-iUCRU8RoQ1kRMAXOCIbHgpV1bF6F7uU-R3ZLob4BU1fYF9u24InRgi0VKOAZ4NQtS0kb-EHGUDVUkYA79jS0';
const privateVapidKey =
 'D6nLUr61cx3P0wuZh5GZlpzHoqTxw5KA7X9APKBJgyM';

webpush.setVapidDetails('mailto:test@test.com', publicVapidKey, privateVapidKey);

app.use(cors());
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, "../backend")));

// Connect database
connectDB();
app.get('/', (req, res) => res.send('API Running'));

// Define routes
app.use('/users', require('./routes/api/users'));
app.use('/auth', require('./routes/api/auth'));
app.use('/clients', require('./routes/api/clients'));
app.use('/experts', require('./routes/api/experts'));
app.use('/articles', require('./routes/api/articles'));
app.use('/courses', require('./routes/api/courses'));
app.use('/diets', require('./routes/api/diets'));
app.use('/tags', require('./routes/api/tags'));
app.use('/objectives', require('./routes/api/objectives'));
app.use('/subscriptions', require('./routes/api/subscriptions'));
app.use('/achievements', require('./routes/api/achievements'));

app.listen(PORT, function() {
  console.log("Server is running on Port: " + PORT);
});
