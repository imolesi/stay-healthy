const mongoose = require('mongoose');
const host = 'mongodb';
const dbPath = 'mongodb://' + host + ':27017/stayhealthy';

const connectDB = async() => {
  try {
    await mongoose.connect(dbPath, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    });
    console.log("MongoDB connected...");
  } catch(err) {
    console.error(err.message);
    process.exit(-1);
  }
};

module.exports = connectDB;
