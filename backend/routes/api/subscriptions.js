const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, oneOf, validationResult } = require('express-validator');
const webpush = require('web-push');
const credentials = require('../../config/mail-credentials.json');
const send = require('gmail-send')({
  user: credentials.user,
  pass: credentials.pass,
  subject: 'StayHealthy'
});

const User = require('../../model/user.model');
const Client = require('../../model/client.model');
const Article = require('../../model/article.model');
const Course = require('../../model/course.model');
const Achievement = require('../../model/achievement.model');

router.get(
  '/article/:id',
  auth,
  async (req, res) => {
    try {
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found' });
      }
      const tags = article.article_tags && Array.isArray(article.article_tags) ? article.article_tags.filter(t => t.tag).map(t => t.tag) : [];

      const clients = await Client.find({"client_interests.tag": { $in: tags } });
      for (const c of clients.filter(c => c.client_user)) {
        const user = await User.findById(c.client_user);
        if (user) {
          if (user.user_email) {
            send({
              subject: 'StayHealthy: Nuovo articolo',
              to: user.user_email,
              text: `È disponibile un nuovo articolo che ti potrebbe interessare: ${req.get('origin')}/articles/${req.params.id}`,
            }, function(err, res) {
              if (err) return console.log('send() callback returned: err:', err);
              console.log('send() callback returned: res:', res);
            });
          }
          if (user.user_subscription) {
            const payload = JSON.stringify({
              title: 'StayHealthy',
              message: 'È disponibile un nuovo articolo che ti potrebbe interessare',
              url: `/articles/${req.params.id}` });
            webpush.sendNotification(user.user_subscription, payload).catch(err => console.error(err.message));
          }
        }
      }
      res.status(200).json({});

    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.get(
  '/course/:id',
  auth,
  async (req, res) => {
    try {
      const course = await Course.findById(req.params.id);
      if (!course) {
        return res.status(400).json({ msg: 'Course not found' });
      }
      const objectives = course.course_objectives && Array.isArray(course.course_objectives) ? course.course_objectives.filter(o => o.objective).map(o => o.objective) : [];

      const clients = await Client.find({"client_objectives.objective": { $in: objectives } });
      for (const c of clients.filter(c => c.client_user)) {
        const user = await User.findById(c.client_user);
        if (user) {
          if (user.user_email) {
            send({
              subject: 'StayHealthy: Nuovo corso',
              to: user.user_email,
              text: `È disponibile un nuovo corso in linea con i tuoi obiettivi: ${req.get('origin')}/courses/${req.params.id}`,
            }, function(err, res) {
              if (err) return console.log('send() callback returned: err:', err);
              console.log('send() callback returned: res:', res);
            });
          }
          if (user.user_subscription) {
            const payload = JSON.stringify({
              title: 'StayHealthy',
              message: 'È disponibile un nuovo corso in linea con i tuoi obiettivi',
              url: `/courses/${req.params.id}` });
            webpush.sendNotification(user.user_subscription, payload).catch(err => console.error(err.message));
          }
        }
      }
      res.status(200).json({});

    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/course-subscribe',
  [
    auth,
    [
      check('course_id', 'Course Id is required')
      .not()
      .isEmpty(),
      check('user_id', 'User Id is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const {
        course_id,
        user_id
      } = req.body;

      const course = await Course.findById(course_id);
      if (!course) {
        return res.status(400).json({ msg: 'Course not found' });
      }
      const user = await User.findById(user_id);
      if (!user || !user._id) {
        return res.status(400).json({ msg: 'User not found' });
      }

      if (user.user_subscription && course.course_endDate && course.course_endDate instanceof Date) {
        const timeToStart = course.course_startDate.getTime() - new Date().getTime();

        if (user.user_email) {
          send({
            subject: 'StayHealthy: Corso iniziato',
            to: user.user_email,
            text: `'Un corso a cui sei iscritto è iniziato o in corso: ${req.get('origin')}/courses/${course_id}`,
          }, function(err, res) {
            if (err) return console.log('send() callback returned: err:', err);
            console.log('send() callback returned: res:', res);
          });

          if (timeToStart - 3600000 > 0) {
            send({
              subject: 'StayHealthy: Inizio corso',
              to: user.user_email,
              text: `Un corso a cui sei iscritto inizia tra un\'ora: ${req.get('origin')}/courses/${course_id}`,
            }, function(err, res) {
              if (err) return console.log('send() callback returned: err:', err);
              console.log('send() callback returned: res:', res);
            });
          }
        }

        if (user.user_subscription) {
          const payload = JSON.stringify({
            title: 'StayHealthy',
            message: 'Un corso a cui sei iscritto è iniziato o in corso',
            url: `/courses/${course_id}`,
            timestamp: timeToStart > 0 && timeToStart
          });
          webpush.sendNotification(user.user_subscription, payload).catch(err => console.error(err.message));

          if (timeToStart - 3600000 > 0) {
            const payload2 = JSON.stringify({
              title: 'StayHealthy',
              message: 'Un corso a cui sei iscritto inizia tra un\'ora',
              url: `/courses/${course_id}`,
              timestamp: timeToStart - 3600000
            });
            webpush.sendNotification(user.user_subscription, payload2).catch(err => console.error(err.message));
          }
        }
      }
      res.status(200).json({});

    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.get(
  '/comment/:id',
  auth,
  async (req, res) => {
    try {
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found' });
      }
      if (!article.article_author) {
        return res.status(400).json({ msg: 'Article author not valid' });
      }
      const author = await User.findById(article.article_author);
      if (author) {
        if (author.user_email) {
          send({
            subject: 'StayHealthy: Nuovo commento',
            to: author.user_email,
            text: `Qualcuno ha commentato un tuo articolo: ${req.get('origin')}/articles/${req.params.id}`,
          }, function(err, res) {
            if (err) return console.log('send() callback returned: err:', err);
            console.log('send() callback returned: res:', res);
          });
        }
        if (author.user_subscription) {
          const payload = JSON.stringify({
            title: 'StayHealthy',
            message: 'Qualcuno ha commentato un tuo articolo',
            url: `/articles/${req.params.id}`
          });
          webpush.sendNotification(author.user_subscription, payload).catch(err => console.error(err.message));
        }
      }
      res.status(200).json({});

    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/achievement/:id',
  [
    auth,
    [
      check('achievement_name', 'Name is required')
      .not()
      .isEmpty(),
      check('achievement_message', 'Message is required')
      .not()
      .isEmpty(),
      oneOf([
        check('achievement_timestamp').not().exists(),
        check('achievement_timestamp').isNumeric()
      ])
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const user = await User.findById(req.params.id);
      if (!user || !user._id) {
        return res.status(400).json({ msg: 'User not found' });
      }
      const client = await Client.findOne({ client_user: user._id });
      if (!client) {
        return res.status(400).json({ msg: 'Client not found' });
      }

      const {
        achievement_name,
        achievement_message,
        achievement_timestamp
      } = req.body;
      if (!achievement_name) {
        return res.status(400).json({ msg: 'Achievement name not valid' });
      }
      if (!achievement_message) {
        return res.status(400).json({ msg: 'Achievement message not valid' });
      }

      const achievement = await Achievement.findOne({ achievement_name: achievement_name });
      if (!achievement) {
        return res.status(400).json({ msg: 'Achievement not found' });
      }
      const obtainedAchievement = {
        achievement: achievement._id
      }

      if (client.client_achievements.filter(a => a.achievement.toString() === achievement._id.toString()).length === 0) {
        if (!client.client_achievements) {
          client.client_achievements = [ obtainedAchievement ]
        } else {
          if (await Client.findOne({ "_id" : user._id, "client_achievements.achievement" : achievement._id })) {
            return res.status(409).json({ msg: achievement_message });
          } else {
            client.client_achievements.unshift(obtainedAchievement);
          }
        }
        await client.save();

        if (user.user_email) {
          send({
            subject: 'StayHealthy: Nuovo achievement',
            to: user.user_email,
            text: `${achievement_message}: ${req.get('origin')}${(user._id ? `/profile/${user._id}` : '/')}`,
          }, function(err, res) {
            if (err) return console.log('send() callback returned: err:', err);
            console.log('send() callback returned: res:', res);
          });
        }
        if (user.user_subscription) {
            const payload = JSON.stringify({
              title: 'StayHealthy',
              message: achievement_message,
              url: (user._id ? `/profile/${user._id}` : '/'),
              timestamp: achievement_timestamp
            });
            webpush.sendNotification(user.user_subscription, payload).catch(err => console.error(err.message));
        }
      }
      res.status(200).json({});
    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);


module.exports = router;
