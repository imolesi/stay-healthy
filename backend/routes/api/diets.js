const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const Diet = require('../../model/diet.model');
const Expert = require('../../model/expert.model');

router.get(
  '/',
  async (req, res) => {
    try {
      const diets = await Diet.find();
      res.json(diets);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.get(
  '/:id',
  async (req, res) => {
    try {
      const diet = await Diet.findById(req.params.id);
      if (!diet) {
        return res.status(400).json({ msg: 'Diet not found '});
      }
      res.json(diet);
    } catch(err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Diet not found '});
      }
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/',
  [ auth,
    [
      check('diet_name', 'Name is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const expert = await Expert.findOne({ expert_user: req.user.id }).select('-password');
      if (!expert) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      const newDiet = new Diet({
        diet_name: req.body.diet_name,
        diet_description: req.body.diet_description,
        diet_author: expert.expert_user
      });
      const diet = await newDiet.save();
      res.json(diet);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/:id',
  auth,
  async (req, res) => {
      const diet = await Diet.findById(req.params.id);
      if (!diet) {
        return res.status(400).json({ msg: 'Diet not found '});
      }
      if (!diet.diet_author || diet.diet_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }

      const {
        diet_name,
        diet_description
      } = req.body;
      const dietFields = {};
      if (diet_name) {
        dietFields.diet_name = diet_name;
      }
      if (diet_description) {
        dietFields.diet_description = diet_description;
      }

      try {
        let diet = await Diet.findById(req.params.id);
        if (!diet) {
          return res.status(400).json({ msg: 'Diet not found '});
        }
        diet = await Diet.findOneAndUpdate(
          { diet_author: req.user.id },
          { $set: dietFields },
          { new: true }
        );
        return res.json(diet);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/:id',
  auth,
  async (req, res) => {
    try {
      const diet = await Diet.findById(req.params.id);
      if (!diet) {
        return res.status(400).json({ msg: 'Diet not found' });
      }
      if (!diet.diet_author || diet.diet_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      await diet.remove();
      res.json({ msg: "Diet removed" });
    } catch(err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Diet not found' });
      }
      res.status(500).send(err.message);
    }
  }
);

router.put(
  '/:id/meal',
  [
    auth,
    [
      check('meal_name', 'Name is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const diet = await Diet.findById(req.params.id);
      if (!diet) {
        return res.status(400).json({ msg: 'Diet not found' });
      }
      if (!diet.diet_author || diet.diet_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      const newMeal = {
        meal_name: req.body.meal_name,
        intake: []
      };
      diet.diet_meals.unshift(newMeal);
      await diet.save();
      res.json(diet);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/:id/meal/:meal_id',
  auth,
  async (req, res) => {
    try {
      const diet = await Diet.findById(req.params.id);
      if (!diet) {
        return res.status(400).json({ msg: 'Diet not found' });
      }
      if (!diet.diet_author || diet.diet_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      const toRemove = diet.diet_meals.map(item => item.id).indexOf(req.params.meal_id);
      diet.diet_meals.splice(toRemove, 1);
      await diet.save();
      res.json(diet);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.put(
  '/:id/meal/:meal_id',
  [
    auth,
    [
      check('nutrient', 'Name is required')
      .not()
      .isEmpty(),
      check('quantity', 'Quantity must have a valid value')
      .isNumeric()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const diet = await Diet.findById(req.params.id);
      if (!diet) {
        return res.status(400).json({ msg: 'Diet not found' });
      }
      if (!diet.diet_author || diet.diet_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      if (!diet.diet_meals) {
        return res.status(400).json({ msg: 'Meal not found' });
      }
      const meal = await diet.diet_meals.find(x => x._id == req.params.meal_id);
      if (!meal) {
        return res.status(400).json({ msg: 'Meal not found' });
      }
      const newIntake = {
        nutrient: req.body.nutrient,
        quantity: req.body.quantity
      };
      meal.intake.unshift(newIntake);
      await diet.save();
      res.json(diet);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/:id/meal/:meal_id/intake/:intake_id',
  auth,
  async (req, res) => {
    try {
      const diet = await Diet.findById(req.params.id);
      if (!diet) {
        return res.status(400).json({ msg: 'Diet not found' });
      }
      if (!diet.diet_author || diet.diet_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      if (!diet.diet_meals) {
        return res.status(400).json({ msg: 'Meal not found' });
      }
      const meal = await diet.diet_meals.find(x => x._id == req.params.meal_id);
      if (!meal) {
        return res.status(400).json({ msg: 'Meal not found' });
      }
      const toRemove = meal.intake.map(item => item.id).indexOf(req.params.intake_id);
      meal.intake.splice(toRemove, 1);
      await diet.save();
      res.json(diet);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

module.exports = router;
