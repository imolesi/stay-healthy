const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, oneOf, validationResult } = require('express-validator');

const Tag = require('../../model/tag.model');
const Article = require('../../model/article.model');
const User = require('../../model/user.model');
const Expert = require('../../model/expert.model');

router.get(
  '/',
  async (req, res) => {
    try {
      const articles = await Article.find().sort( { article_publicationDate: 'descending' });
      res.json(articles);
    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.get(
  '/:id',
  async (req, res) => {
    try {
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found '});
      }
      res.json(article);
    } catch (err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Article not found '});
      }
      res.status(500).send(err.message);
    }
  }
);

// solo gli esperti possono postare articoli (Expert.find)
router.post(
  '/',
  [ auth,
    [
      check('article_title', 'Title is required')
      .not()
      .isEmpty(),
      check('article_description', 'Content is required')
      .not()
      .isEmpty(),
      oneOf([
        check('article_tags').not().exists(),
        [
          check('article_tags').isArray(),
          check('article_tags').isLength({ min: 1 })
        ]
      ])
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const user = await User.findById(req.user.id).select('-password');
      if (!user) {
        return res.status(400).json({ errors: [{ msg: 'User not found' }] });
      }
      const expert = await Expert.findOne({ expert_user: req.user.id }).select('-password');
      if (!expert) {
        return res.status(401).json({ msg: 'User not authorized' });
      }

      if (Array.isArray(req.body.article_tags) && !(Array.isArray(expert.expert_knowledge) && req.body.article_tags.filter(t => t.tag).every(v => expert.expert_knowledge.filter(t => t.tag).map(t => t.tag).includes(v.tag.toString())))) {
        return res.status(401).json({ msg: 'Expert knowledge doesn\'t include every submitted tag',
          expert_knowledge: expert.expert_knowledge,
          article_tags: req.body.article_tags
        });
      }

      const newArticle = new Article({
        article_title: req.body.article_title,
        article_description: req.body.article_description,
        article_author: expert.expert_user,
        article_avatar: user.user_avatar,
        article_tags: req.body.article_tags
      });
      const article = await newArticle.save();
      res.json(article);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/:id',
  [ auth,
    [
      oneOf([
        check('article_tags').not().exists(),
        [
          check('article_tags').isArray(),
          check('article_tags').isLength({ min: 1 })
        ]
      ])
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found '});
      }
      const expert = await Expert.findOne({ expert_user: req.user.id }).select('-password');
      if (!expert || !article.article_author || article.article_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }

      const {
        article_title,
        article_description,
        article_tags
      } = req.body;
      const articleFields = {};
      if (article_title) {
        articleFields.article_title = article_title;
      }
      if (article_description) {
        articleFields.article_description = article_description;
      }
      if (article_tags) {
        articleFields.article_tags = article_tags;
      }

      if (Array.isArray(article_tags) && !(Array.isArray(expert.expert_knowledge) && article_tags.filter(t => t.tag).every(v => expert.expert_knowledge.filter(t => t.tag).map(t => t.tag).includes(v.tag.toString())))) {
        return res.status(401).json({ msg: 'Expert knowledge doesn\'t include every submitted tag',
          expert_knowledge: expert.expert_knowledge,
          article_tags: article_tags
        });
      }

      const updatedArticle = await Article.findOneAndUpdate(
        { _id: req.params.id },
        { $set: articleFields },
        { new: true }
      );
      if (!updatedArticle) {
        return res.status(400).json({ msg: 'Article not found '});
      }
      res.json(await updatedArticle.save());
    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/:id',
  auth,
  async (req, res) => {
    try {
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found' });
      }
      if (!article.article_author || article.article_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      await article.remove();
      res.json({ msg: "Article removed" });
    } catch (err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Article not found' });
      }
      res.status(500).send(err.message);
    }
  }
);

// Tag id refers to the tag id from the tag collection, not to the instance id in the array
router.put(
  '/tag/:id/:tag_id',
  auth,
  async (req, res) => {
    try {
      let tag = await Tag.findById(req.params.tag_id);
      if (!tag) {
        return res.status(400).json({ msg: 'Tag not found' });
      }
      let article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found' });
      }
      if (!article.article_author || article.article_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      const newTag = {
        tag: tag._id
      };
      if (!article.article_tags) {
        article.article_tags = [ newTag ];
      } else {
        if (await Article.findOne({ "_id" : req.params.id, "article_tags.tag" : tag._id })) {
          return res.status(409).json({ msg: 'Tag already present' });
        } else {
          article.article_tags.unshift(newTag);
        }
      }
      await article.save();
      res.json(article.article_tags);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

// Tag id refers to the tag id from the tag collection, not to the instance id in the array
router.delete(
  '/tag/:id/:tag_id',
  auth,
  async (req, res) => {
    try {
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found '});
      }
      if (article.article_author.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      if (article.article_tags) {
        const toRemove = article.article_tags.map(item => item.tag).indexOf(req.params.tag_id);
        if (toRemove !== -1) {
          article.article_tags.splice(toRemove, 1);
        } else {
          return res.status(400).json({ msg: 'Tag not present' });
        }
      } else {
        return res.status(400).json({ msg: 'Tag not present' });
      }
      await article.save();
      res.json(article.article_tags);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);


router.put(
  '/like/:id',
  auth,
  async (req, res) => {
    try {
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found' });
      }
      // Check if already liked
      if (article.article_likes.filter(like => like.user.toString() === req.user.id).length > 0) {
        return res.status(400).json({ msg: 'Article already liked' });
      }
      article.article_likes.unshift({ user: req.user.id });
      await article.save();
      res.json(article.article_likes);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.put(
  '/unlike/:id',
  auth,
  async (req, res) => {
    try {
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found' });
      }
      // Check if already liked
      if (article.article_likes.filter(like => like.user.toString() === req.user.id).length === 0) {
        return res.status(400).json({ msg: 'Article is not liked' });
      }
      const removeIndex = article.article_likes.map(like => like.user.toString()).indexOf(req.user.id);
      article.article_likes.splice(removeIndex, 1);
      await article.save();
      res.json(article.article_likes);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/comments/:id',
  [ auth,
    [
      check('text', 'Content is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const user = await User.findById(req.user.id).select('-password');
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found' });
      }
      const newComment = {
        user: user._id,
        text: req.body.text,
        name: user.user_username,
        avatar: user.user_avatar
      };
      article.article_comments.unshift(newComment);
      await article.save();
      res.json({comments: article.article_comments, user: user._id});
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/comments/:id/:comment_id',
  auth,
  async (req, res) => {
    try {
      const article = await Article.findById(req.params.id);
      if (!article) {
        return res.status(400).json({ msg: 'Article not found' });
      }
      const comment = article.article_comments.find(comment => comment._id.toString() === req.params.comment_id);
      if (!comment) {
        return res.status(400).json({ msg: 'Comment not found' });
      }
      if (comment.user.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      const removeIndex = article.article_comments.map(comment => comment.user.toString()).indexOf(req.user.id);
      article.article_comments.splice(removeIndex, 1);
      await article.save();
      res.json(article.article_comments);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.get(
  '/comments/:id',
  async (req, res) => {
    try {
      let numComments = 0;
      const user = await User.findById(req.params.id);
      if (!user || !user._id) {
        return res.status(400).json({ msg: 'User not found '});
      }
      const articles = await Article.find({ "article_comments.user": user._id })
      articles.forEach(a => {
        if (a.article_comments && a.article_comments.length > 0) {
          numComments = numComments + a.article_comments.filter(c => c.user && c.user.toString() === user._id.toString()).length;
        }
      });
      res.json({ numComments });
    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

module.exports = router;
