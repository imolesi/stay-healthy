const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../../config/default.json');
const { check, validationResult } = require('express-validator');

const User = require('../../model/user.model');

router.post(
  '/register',
  [
    check('user_email', 'Inserisci un indirizzo e-mail valido')
    .isEmail(),
    check('user_username', 'L\'username è obbligatorio')
    .not()
    .isEmpty(),
    check('user_name', 'Il nome è obbligatorio')
    .not()
    .isEmpty(),
    check('user_birthDate').isISO8601().toDate(),
    check('user_lastname', 'Il cognome è obbligatorio')
    .not()
    .isEmpty(),
    check('user_password', 'La password deve contenere almeno 6 caratteri')
    .isLength({ min: 6 })
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      // Check if user already exists
      let user = await User.findOne({ user_email : req.body.user_email });
      if (user) {
        return res.status(400).json({ errors: [{ msg: 'L\'indirizzo email inserito è già utilizzato'}] });
      }
      user = await User.findOne({ user_username : req.body.user_username });
      if (user) {
        return res.status(400).json({ errors: [{ msg: 'Il nome utente inserito è già utilizzato'}] });
      }

      // Get user gravatar
      const avatar = gravatar.url(req.body.user_email, {
        s: '200',
        r: 'pg',
        d: 'mm'
      });

      // Encryt password

      const salt = await bcrypt.genSalt(10);
      const hashedPassword = await bcrypt.hash(req.body.user_password, salt);

      user = new User({
        user_email : req.body.user_email,
        user_username : req.body.user_username,
        user_name : req.body.user_name,
        user_lastname : req.body.user_lastname,
        user_gender: req.body.user_gender,
        user_password : hashedPassword,
        user_birthDate : req.body.user_birthDate,
        user_avatar : avatar
      });
      await user.save();

      // Return jsonwebtoken (login after registration)

      const payload = {
        user: {
          id: user._id
        }
      };

      jwt.sign(
        payload,
        config.jwtToken,
        { expiresIn: 3600 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );

    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }

  }
);

router.post(
  '/subscriptions/:id',
  auth,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      let user = await User.findById(req.params.id);
      if (!user) {
        return res.status(400).json({ msg: 'User not found' });
      }
      if (req.params.id !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      let subscription = req.body;
      if (!subscription.endpoint || !subscription.keys || !subscription.keys.p256dh || !subscription.keys.auth) {
        return res.status(400).json({ msg: 'Subscription not valid' });
      }
      try {
        const updatedUser = await User.findOneAndUpdate(
          { _id: req.params.id },
          { $set: { user_subscription: subscription } },
          { new: true }
        );
        if (!updatedUser) {
          return res.status(400).json({ msg: 'User not found '});
        }
        const user = await updatedUser.save();
        res.json(user);
      } catch (err) {
        console.error(err.message);
        res.status(500).send(err.message);
      }
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }

  }
);

router.delete(
  '/subscriptions/:id',
  auth,
  async (req, res) => {
    try {
      let user = await User.findById(req.params.id);
      if (!user) {
        return res.status(400).json({ msg: 'User not found' });
      }
      if (req.params.id !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      let updatedUser = await User.findOneAndUpdate(
        { _id: req.params.id },
        { $set: { _id: req.params.id }, $unset: { user_subscription: "" } },
        { new: true, upsert: true }
      );
      if (!updatedUser) {
        return res.status(400).json({ msg: 'User not found '});
      }
      await updatedUser.save();
      res.json({ msg: "Subscription removed" });
    } catch (err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'User not found' });
      }
      res.status(500).send(err.message);
    }
  }
);

module.exports = router;
