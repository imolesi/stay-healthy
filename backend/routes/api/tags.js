const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const Tag = require('../../model/tag.model');

router.get(
  '/',
  async (req, res) => {
    try {
      const tags = await Tag.find();
      res.json(tags);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/',
  [ auth,
    [
      check('tag_name', 'Name is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const newTag = new Tag({
        tag_name: req.body.tag_name
      });
      const tag = await newTag.save();
      res.json(tag);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/:id',
  auth,
  async (req, res) => {
    try {
      const tag = await Tag.findById(req.params.id);
      if (!tag) {
        return res.status(400).json({ msg: 'Tag not found' });
      }
      await tag.remove();
      res.json({ msg: "Tag removed" });
    } catch(err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Tag not found' });
      }
      res.status(500).send(err.message);
    }
  }
);

module.exports = router;
