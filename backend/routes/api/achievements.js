const express = require('express');
const router = express.Router();
/* const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator'); */

const Achievement = require('../../model/achievement.model');

router.get(
  '/',
  async (req, res) => {
    try {
      const achievements = await Achievement.find().sort( { date: 1 });
      res.json(achievements);
    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.get(
  '/:id',
  async (req, res) => {
    try {
      const achievement = await Achievement.findById(req.params.id);
      if (!achievement) {
        return res.status(400).json({ msg: 'Achievement not found '});
      }
      res.json(achievement);
    } catch (err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Achievement not found '});
      }
      res.status(500).send(err.message);
    }
  }
);

/*
router.post(
  '/',
  [
    [
      check('achievement_name', 'Name is required')
      .not()
      .isEmpty(),
      check('achievement_description', 'Description is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const newAchievement = new Achievement({
        achievement_name: req.body.achievement_name,
        achievement_description: req.body.achievement_description,
        achievement_image: req.body.achievement_image
      });
      const achievement = await newAchievement.save();
      res.json(achievement);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/:id',
  async (req, res) => {
      const achievement = await Achievement.findById(req.params.id);
      if (!achievement) {
        return res.status(400).json({ msg: 'Achievement not found '});
      }

      const {
        achievement_name,
        achievement_description,
        achievement_image
      } = req.body;
      const achievementFields = {};
      if (achievement_name) {
        achievementFields.achievement_name = achievement_name;
      }
      if (achievement_description) {
        achievementFields.achievement_description = achievement_description;
      }
      if (achievement_image) {
        achievementFields.achievement_image = achievement_image;
      }

      try {
        const updatedAchievement = await Achievement.findOneAndUpdate(
          { _id: req.params.id },
          { $set: achievementFields },
          { new: true }
        );
        if (!updatedAchievement) {
          return res.status(400).json({ msg: 'Achievement not found '});
        }
        const achievement = await updatedAchievement.save();
        res.json(achievement);
      } catch (err) {
        console.error(err.message);
        res.status(500).send(err.message);
      }
  }
);

router.delete(
  '/:id',
  async (req, res) => {
    try {
      const achievement = await Achievement.findById(req.params.id);
      if (!achievement) {
        return res.status(400).json({ msg: 'Achievement not found' });
      }
      await achievement.remove();
      res.json({ msg: "Achievement removed" });
    } catch (err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Achievement not found' });
      }
      res.status(500).send(err.message);
    }
  }
);
*/

module.exports = router;