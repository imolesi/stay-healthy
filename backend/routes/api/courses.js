const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, oneOf, validationResult } = require('express-validator');

const Course = require('../../model/course.model');
const User = require('../../model/user.model');
const Expert = require('../../model/expert.model');
const Objective = require('../../model/objective.model');

router.get(
  '/',
  async (req, res) => {
    try {
      const courses = await Course.find().sort( { course_publicationDate: 'descending' });
      res.json(courses);
    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.get(
  '/:id',
  async (req, res) => {
    try {
      const course = await Course.findById(req.params.id);
      if (!course) {
        return res.status(400).json({ msg: 'Course not found '});
      }
      res.json(course);
    } catch (err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Course not found '});
      }
      res.status(500).send(err.message);
    }
  }
);

// solo gli esperti possono postare corsi
router.post(
  '/',
  [ auth,
    [
      check('course_name', 'Course name is required')
      .not()
      .isEmpty(),
      check('course_description', 'Course description is required')
      .not()
      .isEmpty(),
      check('course_startDate').isISO8601().toDate(),
      check('course_endDate').isISO8601().toDate(),
      oneOf([
        check('course_days', 'Course days may not exist')
        .not()
        .exists(),
        [
          check('course_days', 'Course days must not be an empty array')
          .isLength({ min: 1 }),
          check('course_days.*', 'Course days must be >= 0 and <= 6')
          .isIn([1, 2, 3, 4, 5, 6, 0])
        ]
      ]),
      oneOf([
        check('course_objectives').not().exists(),
        [
          check('course_objectives').isArray(),
          check('course_objectives').isLength({ min: 1 })
        ]
      ])
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const user = await User.findById(req.user.id).select('-password');
      if (!user) {
        return res.status(400).json({ errors: [{ msg: 'User not found' }] });
      }
      const expert = await Expert.findOne({ expert_user: req.user.id }).select('-password');
      if (!expert) {
        return res.status(401).json({ msg: 'User not authorized' });
      }

      if (Array.isArray(req.body.course_objectives) && !(Array.isArray(expert.expert_objectives) && req.body.course_objectives.filter(o => o.objective).every(v => expert.expert_objectives.filter(o => o.objective).map(o => o.objective).includes(v.objective.toString())))) {
        return res.status(401).json({ msg: 'Expert objectives don\'t include every submitted objective',
          expert_objectives: expert.expert_objectives,
          course_objectives: req.body.course_objectives
        });
      }

      const newCourse = new Course({
        course_name: req.body.course_name,
        course_description: req.body.course_description,
        course_trainer: expert.expert_user,
        course_avatar: user.user_avatar,
        course_startDate: req.body.course_startDate,
        course_endDate: req.body.course_endDate,
        course_days: req.body.course_days,
        course_objectives: req.body.course_objectives
      });
      const course = await newCourse.save();
      res.json(course);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/:id',
  [ auth,
    [
      oneOf([
        check('course_startDate').not().exists(),
        check('course_startDate').isISO8601().toDate(),
      ]),
      oneOf([
        check('course_endDate').not().exists(),
        check('course_endDate').isISO8601().toDate(),
      ]),
      oneOf([
        check('course_days', 'Course days may not exist')
        .not()
        .exists(),
        [
          check('course_days', 'Course days must not be an empty array')
          .isLength({ min: 1 }),
          check('course_days.*', 'Course days must be >= 0 and <= 6')
          .isIn([1, 2, 3, 4, 5, 6, 7])
        ]
      ]),
      oneOf([
        check('course_objectives').not().exists(),
        [
          check('course_objectives').isArray(),
          check('course_objectives').isLength({ min: 1 })
        ]
      ])
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const course = await Course.findById(req.params.id);
      if (!course) {
        return res.status(400).json({ msg: 'Course not found' });
      }
      const expert = await Expert.findOne({ expert_user: req.user.id }).select('-password');
      if (!expert || !course.course_trainer || course.course_trainer.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }

      const {
        course_name,
        course_description,
        course_startDate,
        course_endDate,
        course_days,
        course_objectives
      } = req.body;
      const courseFields = {};
      if (course_name) {
        courseFields.course_name = course_name;
      }
      if (course_description) {
        courseFields.course_description = course_description;
      }
      if (course_startDate) {
        courseFields.course_startDate = course_startDate;
      }
      if (course_endDate) {
        courseFields.course_endDate = course_endDate;
      }
      if (course_days) {
        courseFields.course_days = course_days;
      }
      if (course_objectives) {
        courseFields.course_objectives = course_objectives;
      }

      if (Array.isArray(course_objectives) && !(Array.isArray(expert.expert_objectives) && course_objectives.filter(o => o.objective).every(v => expert.expert_objectives.filter(o => o.objective).map(o => o.objective).includes(v.objective.toString())))) {
        return res.status(401).json({ msg: 'Expert objectives don\'t include every submitted objective',
          expert_objectives: expert.expert_objectives,
          course_objectives: req.body.course_objectives
        });
      }

      const updatedCourse = await Course.findOneAndUpdate(
        { _id: req.params.id },
        { $set: courseFields, $unset: course_days ? {} : { course_days: "" } },
        { new: true }
      );
      if (!updatedCourse) {
        return res.status(400).json({ msg: 'Course not found '});
      }
      res.json(await updatedCourse.save());
    } catch (err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/:id',
  auth,
  async (req, res) => {
    try {
      const course = await Course.findById(req.params.id);
      if (!course) {
        return res.status(400).json({ msg: 'Course not found' });
      }
      if (!course.course_trainer || course.course_trainer.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      await course.remove();
      res.json({ msg: "Course removed" });
    } catch (err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Course not found' });
      }
      res.status(500).send(err.message);
    }
  }
);

router.put(
  '/subscribe/:id',
  auth,
  async (req, res) => {
    try {
      const course = await Course.findById(req.params.id);
      if (!course) {
        return res.status(400).json({ msg: 'Course not found' });
      }
      const user = await User.findById(req.user.id).select('-password');
      if (!user) {
        return res.status(400).json({ msg: 'User not found' });
      }
      const subscribe_expiree = new Date(new Date(course.course_endDate).toISOString().substr(0, 10));
      subscribe_expiree.setDate(subscribe_expiree.getDate() + 1);

      if (new Date() >= subscribe_expiree) {
        return res.status(401).json({ msg: 'User can\'t subscribe after course period' });
      }
      const newSubscriber = {
        subscriber: user._id
      };
      if (!course.course_subscribers) {
        course.course_subscribers = [ newSubscriber ];
      } else {
        if (await Course.findOne({ "_id" : req.params.id, "course_subscribers.subscriber" : user._id})) {
          return res.status(409).json({ msg: 'User already present' });
        } else {
          course.course_subscribers.unshift(newSubscriber);
        }
      }
      await course.save();
      res.json({ subscribers: course.course_subscribers, subscribedId: req.user.id });
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/subscribe/:id',
  auth,
  async (req, res) => {
    try {
      const course = await Course.findById(req.params.id);
      if (!course) {
        return res.status(400).json({ msg: 'Course not found' });
      }
      if (course.course_subscribers) {
        const toRemove = course.course_subscribers.map(item => item.subscriber).indexOf(req.user.id);
        if (toRemove !== -1) {
          course.course_subscribers.splice(toRemove, 1);
        } else {
          return res.status(400).json({ msg: 'User not present' });
        }
      } else {
        return res.status(400).json({ msg: 'User not present' });
      }
      await course.save();
      res.json({...course.course_subscribers, unsubscribed: req.user.id});
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

// Objective id refers to the objective id from the objective collection, not to the instance id in the array
router.put(
  '/objectives/:id/:objective_id',
  auth,
  async (req, res) => {
    try {
      const course = await Course.findById(req.params.id);
      if (!course) {
        return res.status(400).json({ msg: 'Course not found' });
      }
      const objective = await Objective.findById(req.params.objective_id);
      if (!objective) {
        return res.status(400).json({ msg: 'Objective not found '});
      }
      if (!course.course_trainer || course.course_trainer.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      const newObjective = {
        objective: objective._id
      };
      if (!course.course_objectives) {
        course.course_objectives = [ newObjective ];
      } else {
        if (await Course.findOne({ "_id" : req.params.id, "course_objectives.objective" : objective._id })) {
          return res.status(409).json({ msg: 'Objective already present' });
        } else {
          course.course_objectives.unshift(newObjective);
        }
      }
      await course.save();
      res.json(course.course_objectives);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

// Objective id refers to the objective id from the objective collection, not to the instance id in the array
router.delete(
  '/objectives/:id/:objective_id',
  auth,
  async (req, res) => {
    try {
      const course = await Course.findById(req.params.id);
      if (!course) {
        return res.status(400).json({ msg: 'Course not found' });
      }
      const objective = await Objective.findById(req.params.objective_id)
      if (!objective) {
        return res.status(400).json({ msg: 'Objective not found '});
      }
      if (!course.course_trainer || course.course_trainer.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      if (course.course_objectives) {
        const toRemove = course.course_objectives.map(item => item.objective).indexOf(req.params.objective_id);
        if (toRemove !== -1) {
          course.course_objectives.splice(toRemove, 1);
        } else {
          return res.status(400).json({ msg: 'Objective not present'+course.course_objectives.map(item => item.objective).indexOf(req.params.objective_id) });
        }
      } else {
        return res.status(400).json({ msg: 'Objective not present' });
      }
      await course.save();
      res.json(course.course_objectives);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

module.exports = router;
