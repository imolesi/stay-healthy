const express = require('express');
const router  = express.Router();
const auth = require('../../middleware/auth');
const { check, oneOf, validationResult } = require('express-validator');

const Expert = require('../../model/expert.model');

router.get(
  '/me',
  auth,
  async (req, res) => {
    try {
      const profile = await Expert.findOne({ expert_user: req.user.id })
      .populate('expert_user', ['user_name', 'user_lastname', 'user_avatar']);
      if (!profile) {
        return res.status(400).json({ msg: 'This user is not an expert'});
      }
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/',
  [
    auth,
    [
      check('expert_bio', 'Inserisci la biografia')
      .not()
      .isEmpty(),
      oneOf([
        check('expert_knowledge').not().exists(),
        check('expert_knowledge').isArray()
      ]),
      oneOf([
        check('expert_objectives').not().exists(),
        check('expert_objectives').isArray()
      ])
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty) {
      return res.status(400).json({ errors: errors.array });
    }

    const {
      expert_bio,
      expert_knowledge,
      expert_objectives
    } = req.body;

    // Build profile object
    const profileFields = {};

    profileFields.expert_user = req.user.id;
    if (expert_bio) {
      profileFields.expert_bio = expert_bio;
    }
    if (expert_knowledge) {
      profileFields.expert_knowledge = expert_knowledge;
    }
    if (expert_objectives) {
      profileFields.expert_objectives = expert_objectives;
    }

    try {
      let profile = await Expert.findOneAndUpdate(
        { expert_user: req.user.id },
        { $set: profileFields },
        { new: true, upsert: true }
      );
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }

  }
);

router.get(
  '/',
  async (req, res) => {
    try {
      const profiles = await Expert.find()
      .populate('expert_user', ['user_name', 'user_lastname', 'user_avatar']);
      res.json(profiles);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.get(
  '/:id',
  async (req, res) => {
    try {
      const profile = await Expert.findOne({ expert_user: req.params.id })
        .populate('expert_user', ["user_name", "user_lastname", "user_avatar"]);
      if (!profile) {
        return res.status(400).json({ msg: 'Expert not found for this user'});
      }
      res.json(profile);
    } catch(err) {
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Expert not found for this user'});
      }
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/',
  auth,
  async (req, res) => {
    try {
      const expert = await Expert.findOneAndRemove({ expert_user: req.user.id });
      if (!expert) {
        return res.status(400).json({ msg: 'Expert not found for this user'});
      }
      // await User.findByIdAndRemove(req.user.id);
      res.json(expert);
    } catch(err) {
      console.error(err.message);
        res.status(500).send(err.message);
    }
  }
);

router.put(
  '/knowledge',
  [
    auth,
    [
      check('tag', 'Valid tag is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const {
      tag
    } = req.body;
    const newKnowledge = {
      tag
    };
    try {
      const profile = await Expert.findOne({ expert_user: req.user.id });
      if (!profile) {
        return res.status(400).json({ msg: 'Expert not found for this user'});
      }
      if (!profile.expert_knowledge) {
        profile.expert_knowledge = [ newKnowledge ];
      } else {
        profile.expert_knowledge.unshift(newKnowledge);
      }
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/knowledge/:id',
  auth,
  async (req, res) => {
    try {
      const profile = await Expert.findOne({ expert_user: req.user.id });
      if (!profile) {
        return res.status(400).json({ msg: 'Expert not found for this user'});
      }
      if (profile.expert_knowledge) {
        const toRemove = profile.expert_knowledge.map(item => item._id).indexOf(req.params.id);
        if (toRemove !== -1) {
          profile.expert_knowledge.splice(toRemove, 1);
        }
      }
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.put(
  '/objective',
  [
    auth,
    [
      check('objective', 'Valid objective is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const {
      objective
    } = req.body;
    const newObjective = {
      objective
    };
    try {
      const profile = await Expert.findOne({ expert_user: req.user.id });
      if (!profile) {
        return res.status(400).json({ msg: 'Expert not found for this user' });
      }
      if (!profile.expert_objectives) {
        profile.expert_objectives = [ newObjective ];
      } else {
        profile.expert_objectives.unshift(newObjective);
      }
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/objective/:id',
  auth,
  async (req, res) => {
    try {
      const profile = await Expert.findOne({ expert_user: req.user.id });
      if (!profile) {
        return res.status(400).json({ msg: 'Expert not found for this user'});
      }
      if (profile.expert_objectives) {
        const toRemove = profile.expert_objectives.map(item => item._id).indexOf(req.params.id);
        if (toRemove !== -1) {
          profile.expert_objectives.splice(toRemove, 1);
        }
      }
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

module.exports = router;
