const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const Objective = require('../../model/objective.model');

router.get(
  '/',
  async (req, res) => {
    try {
      const objectives = await Objective.find();
      res.json(objectives);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/',
  [ auth,
    [
      check('objective_name', 'Name is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json( { errors: errors.array() });
    }
    try {
      const newObjective = new Objective({
        objective_name: req.body.objective_name
      });
      const objective = await newObjective.save();
      res.json(objective);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/:id',
  auth,
  async (req, res) => {
    try {
      const objective = await Objective.findById(req.params.id);
      if (!objective) {
        return res.status(400).json({ msg: 'Objective not found' });
      }
      await objective.remove();
      res.json({ msg: "Objective removed" });
    } catch(err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Objective not found' });
      }
      res.status(500).send(err.message);
    }
  }
);

module.exports = router;
