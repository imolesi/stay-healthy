const express = require('express');
const router  = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const User = require('../../model/user.model');
const Client = require('../../model/client.model');
const Article = require('../../model/article.model');

router.get(
  '/me',
  auth,
  async (req, res) => {
    try {
      const profile = await Client.findOne({ client_user: req.user.id })
      .populate('client_user', ['user_name', 'user_lastname', 'user_avatar']);
      if (!profile) {
        return res.status(400).json({ msg: 'This user is not a client'});
      }
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.post(
  '/',
  [
    auth,
    [
      check('client_height', 'Inserisci l\'altezza')
      .not()
      .isEmpty(),
      check('client_objectiveWeight', 'Inserisci il peso desiderato')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty) {
      return res.status(400).json({ errors: errors.array });
    }

    const {
      client_height,
      client_objectiveWeight,
      client_interests,
      client_objectives,
      client_diet
    } = req.body;

    // Build profile object
    const profileFields = {};

    profileFields.client_user = req.user.id;
    if (client_height) {
      profileFields.client_height = client_height;
    }
    if (client_objectiveWeight) {
      profileFields.client_objectiveWeight = client_objectiveWeight;
    }
    if (client_interests) {
      profileFields.client_interests = client_interests;
    }
    if (client_objectives) {
      profileFields.client_objectives = client_objectives;
    }
    if (client_diet) {
      profileFields.client_diet = client_diet;
    }

    try {
      let profile = await Client.findOneAndUpdate(
        { client_user: req.user.id },
        { $set: profileFields, $unset: client_diet ? {} : { client_diet: "" } },
        { new: true, upsert: true }
      );
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }

  }
);

router.get(
  '/',
  async (req, res) => {
    try {
      const profiles = await Client.find()
      .populate('client_user', ['user_name', 'user_lastname', 'user_avatar']);
      res.json(profiles);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.get(
  '/:id',
  async (req, res) => {
    try {
      const profile = await Client.findOne({ client_user: req.params.id })
      .populate('client_user', ["user_name", "user_lastname", "user_avatar"]);
      if (!profile) {
        return res.status(400).json({ msg: 'Profile not found for this user'});
      }
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res.status(400).json({ msg: 'Profile not found for this user'});
      }
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/',
  auth,
  async (req, res) => {
    try {
      await Article.deleteMany( { article_author: req.user.id });
      await Client.findOneAndRemove({ client_user: req.user.id });
      await User.findByIdAndRemove(req.user.id);
      res.json({ msg: 'User deleted' });
    } catch(err) {
      console.error(err.message);
        res.status(500).send(err.message);
    }
  }
);

router.put(
  '/interest',
  [
    auth,
    [
      check('tag', 'Valid tag is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const {
      tag
    } = req.body;
    const newInterest = {
      tag
    };
    try {
      const profile = await Client.findOne({ client_user: req.user.id });
      profile.client_interests.unshift(newInterest);
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/interest/:id',
  auth,
  async (req, res) => {
    try {
      const profile = await Client.findOne({ client_user: req.user.id });
      const toRemove = profile.client_interests.map(item => item.id).indexOf(req.params.id);
      profile.client_interests.splice(toRemove, 1);
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.put(
  '/objective',
  [
    auth,
    [
      check('objective', 'Valid objective is required')
      .not()
      .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const {
      objective
    } = req.body;
    const newObjective = {
      objective
    };
    try {
      const profile = await Client.findOne({ client_user: req.user.id });
      profile.client_objectives.unshift(newObjective);
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/objective/:id',
  auth,
  async (req, res) => {
    try {
      const profile = await Client.findOne({ client_user: req.user.id });
      const toRemove = profile.client_objectives.map(item => item.id).indexOf(req.params.id);
      profile.client_objectives.splice(toRemove, 1);
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.put(
  '/weightProgress',
  [
    auth,
    [
      check('weight', 'Valid weight value is required')
      .isNumeric(),
      check('date').isISO8601().toDate()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const profile = await Client.findOne({ client_user: req.user.id });
      const newProgress = {
        weight: req.body.weight,
        date: req.body.date
      };
      profile.client_weightProgress.push(newProgress);
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

router.delete(
  '/weightProgress/:id',
  auth,
  async (req, res) => {
    try {
      const profile = await Client.findOne({ client_user: req.user.id });
      const toRemove = profile.client_weightProgress.map(item => item.id).indexOf(req.params.id);
      profile.client_weightProgress.splice(toRemove, 1);
      await profile.save();
      res.json(profile);
    } catch(err) {
      console.error(err.message);
      res.status(500).send(err.message);
    }
  }
);

module.exports = router;
