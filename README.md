# StayHealthy
Il progetto proposto consiste nella realizzazione di un'applicazione web che permetta agli utenti di fruire di contenuti in ambito wellness, coprendo ambiti come sport, mental training, alimentazione e altro così da poter favorire il proprio benessere psicofisico. 

I contenuti sono disponibili in forma di articoli, approfondimenti, corsi e attività ed è possibile tenere traccia dei propri progressi.

Il servizio permette anche a diversi esperti di pubblicare i loro contenuti e ricevere feedback dal resto della community che fruisce dell'applicazione.

## Deployment

1. Scaricare da https://bitbucket.org/imolesi/stay-healthy le immagini di web-api, client e database;
2. Opzionale: è possibile scaricare dal medesimo link l'intero codice sorgente nel caso si voglia creare una propria build. Eseguire da linea di comando la seguente istruzione per compilare le immagini necessarie:
	
    ```docker-compose build```

3. Eseguire da linea di comando la seguente istruzione per creare i container ottenuti per mettere in esecuzione i servizi negli appositi container:

    ```docker-compose up```
	
4. Eseguire da linea di comando la seguente istruzione per utilizzare la bash del container MongoDB:

	```docker exec -it "stay-healthy_mongodb_1" bash```

5. Eseguire in ordine le seguenti istruzioni per popolare il database stayhealthy con le collections predefinite:

	```mongorestore -c achievements -d stayhealthy achievements.bson
	mongorestore -c articles -d stayhealthy articles.bson
	mongorestore -c clients -d stayhealthy clients.bson
	mongorestore -c courses -d stayhealthy courses.bson
	mongorestore -c diets -d stayhealthy diets.bson
	mongorestore -c experts -d stayhealthy experts.bson
	mongorestore -c objectives -d stayhealthy objectives.bson
	mongorestore -c tags -d stayhealthy tags.bson
	mongorestore -c users -d stayhealthy users.bson```

6. Il deployment andrà a buon fine e si aprirà automaticamente sul browser predefinito la pagina principale del client. Nel caso ciò non avvenisse, bisognerà connettersi manualmente al seguente indirizzo:

    ```https://localhost:3000```

7. È possibile interagire direttamente con la web-api inoltrando le richieste al seguente indirizzo:

    ```https://localhost:4000```
	
	
## Autori
 
Alessandro Oliva - alessandro.oliva4@studio.unibo.it  
Andrea Betti - andrea.betti10@studio.unibo.it  