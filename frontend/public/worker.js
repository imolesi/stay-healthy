const logo = "./logo.png";

const showNotification = (data) => {
  const body = data.message;
  self.registration.showNotification(data.title, {
    body: body,
    icon: logo,
    data: {
      url: data.url
    },
    actions: [{action: "open_url", title: "Clicca qui"}]
  });
};

self.addEventListener('push', e => {
  const data = e.data.json();
  if (!data.timestamp) {
    showNotification(data);
  } else {
    e.waitUntil(new Promise(resolve => {
      setTimeout(() => {
          showNotification(data);
          resolve();
      }, data.timestamp);
    }));
  }
});

self.addEventListener('notificationclick', e => {
  switch (e.action){
    case 'open_url':
      self.clients.openWindow(e.notification.data.url);
      break;
  }
});
