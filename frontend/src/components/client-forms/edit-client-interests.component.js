import React, { Fragment, useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addInterest, getCurrentClient, getClients } from '../../actions/client'
import { getInterests } from '../../actions/tag';

const EditClientInterests = ({
  getInterests,
  addInterest,
  getCurrentClient,
  getClients,
  auth: { user, isAuthenticated },
  history,
  tags: { tags },
  client: { client, clients } }) => {

  const [formData, setFormData] = useState({
    client_interests: {}
  });

  const {
    client_interests
  } = formData;

  useEffect(() => {
    getClients();
  }, [getClients])
  useEffect(() => {
    if (isAuthenticated && user && clients && clients.find(e => e.client_user && e.client_user._id === user._id)) {
      getCurrentClient();
    }
    getInterests();
  }, [getInterests, getCurrentClient, isAuthenticated, user, clients])

  const handleSelect = (e, tag) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      client_interests: {
        ...state.client_interests,
        [tag._id]: checked
      }
    }))
  }

  const onSubmit = async e => {
    e.preventDefault();
    for (var key in client_interests) {
      if (client_interests[key]) {
        await addInterest({ client_interest: key }, history); // put request per ogni interesse
      };
    }
  }

  return (
    <Fragment>
      <p className="lead">
        <i className="fas fa-exclamation"></i> Indica quali sono i tuoi interessi
          così da essere sempre aggiornato in tempo reale sui tuoi contenuti
      </p>
      <form className="form" onSubmit={e => onSubmit(e)}>
        {
          client &&
          tags
          .filter(e => !client.client_interests.map(f => f.tag.toString()).includes(e._id.toString()))
          .sort()
          .map((tag, id) => {
            const tagCheck = client_interests[tag._id]
            return (
              <label className="m-1 cb-item" key={id}>
                {tag.tag_name}
              <input
                name={tag.tag_name}
                type="checkbox"
                checked={tagCheck ? tagCheck : false}
                onChange={e => handleSelect(e, tag)}
                className="mx-3"
              /></label>
            );
          })
        }
        <small className="form-text">
          Ogni volta che sarà pubblicato un contenuto inerente sarai notificato in tempo reale
        </small>
        <input type="submit" className="btn btn-primary my-1" value="Aggiungi" />
      </form>
    </Fragment>
  )
}

EditClientInterests.propTypes = {
  getInterests: PropTypes.func.isRequired,
  addInterest: PropTypes.func.isRequired,
  getCurrentClient: PropTypes.func.isRequired,
  getClients: PropTypes.func.isRequired,
  tags: PropTypes.object.isRequired,
  client: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  tags: state.tags,
  client: state.client
});

export default connect(mapStateToProps, { getInterests, addInterest, getCurrentClient, getClients })(withRouter(EditClientInterests))
