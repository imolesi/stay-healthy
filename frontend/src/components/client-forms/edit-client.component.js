import React, { Fragment, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import { createClient, getCurrentClient, getClients } from '../../actions/client';
import { getDiets } from '../../actions/diet';
import Diet from '../layout/diet.component';

const EditClient = ({ getCurrentClient, getDiets, getClients, client: { client, clients, loading }, auth: { user, isAuthenticated }, createClient, history, diet }) => {

  const [formData, setFormData] = useState({
    client_height: '',
    client_objectiveWeight: '',
    client_diet: ''
  });

  useEffect(() => {
    getClients();
  }, [getClients])
  useEffect(() => {
    if (isAuthenticated && user && clients && clients.find(e => e.client_user && e.client_user._id === user._id)) {
      getCurrentClient();
    }
  }, [getCurrentClient, isAuthenticated, user, clients]);
  useEffect(() => {
    getDiets();
    setFormData({
      client_height: loading || !client || !client.client_height ? '' : client.client_height,
      client_objectiveWeight: loading || !client || !client.client_objectiveWeight ? '' : client.client_objectiveWeight,
      client_diet: loading || !client || !client.client_diet ? '' : client.client_diet
    });
  }, [loading, client, getDiets]);

  const {
    client_height,
    client_objectiveWeight,
    client_diet
  } = formData;

  const onChange = e => setFormData({
    ...formData, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    createClient(formData, history, true);
  }

  const d = client_diet && diet.diets.length > 0 && diet.diets.find(e => e._id === client_diet && e.diet_name);

  return (
    loading ? <Spinner /> : (<Fragment>
      <p className="lead">
        <i className="fas fa-user"> Modifica la tua situazione corrente</i>
      </p>
      <small>* campo obbligatorio</small>
      <form className="form" onSubmit={e => onSubmit(e)}>
        <div className="form-group">
          <label htmlFor="client_height_edit" className="d-none">Modifica la tua altezza</label>
          <input
            type="number"
            placeholder="* Altezza"
            id="client_height_edit"
            name="client_height"
            value={client_height}
            onChange={e => onChange(e)}
            required
          />
          <small className="form-text">
            La tua altezza corrente
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="client_objectiveWeight_edit" className="d-none">Modifica il tuo peso desiderato</label>
          <input
            type="number"
            placeholder="* Peso desiderato"
            id="client_objectiveWeight_edit"
            name="client_objectiveWeight"
            value={client_objectiveWeight}
            onChange={e => onChange(e)}
            required
          />
          <small className="form-text">
            Il peso che vorresti raggiungere
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="client_diet_edit" className="d-none">Seleziona dieta</label>
          <select id="client_diet_edit" name="client_diet" value={
            client ? client_diet : '0'}
              onChange={e => onChange(e)}>
            <option key="0" value="0" disabled="disabled">Seleziona dieta</option>
            {
              diet.diets.map(function(diet) {
                return (<option key={diet._id} value={diet._id}>{diet.diet_name}</option>);
              })
            }
            <option key="-1" value="">Nessuna</option>
          </select>
        </div>
        {d && <Diet diet={d} />}
        <small className="form-text">
          Potrai modificarla in futuro
        </small>
        <input type='submit' value="Aggiorna" className='btn btn-primary my-1' />
      </form>
    </Fragment>)
  );
};

EditClient.propTypes = {
  getDiets: PropTypes.func.isRequired,
  getCurrentClient: PropTypes.func.isRequired,
  getClients: PropTypes.func.isRequired,
  createClient: PropTypes.func.isRequired,
  client: PropTypes.object.isRequired,
  diet: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  client: state.client,
  diet: state.diet
});

export default connect(mapStateToProps, { getClients, getCurrentClient, createClient, getDiets })(withRouter(EditClient));
