import React, { Fragment, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createClient } from '../../actions/client';
import { getInterests } from '../../actions/tag';
import { getObjectives } from '../../actions/objective';
import { getDiets } from '../../actions/diet';
import { setAlert } from '../../actions/alert';

const CreateClient = ({ getInterests, getObjectives, getDiets, setAlert, createClient, tags: { tags }, objectives: { objectives }, diet, history }) => {
  const [formData, setFormData] = useState({
    client_height: '',
    client_objectiveWeight: '',
    client_interests: [],
    client_objectives: [],
    client_diet: ''
  });

  const {
    client_height,
    client_objectiveWeight,
    client_interests,
    client_objectives,
    client_diet
  } = formData;

  const handleInterestsSelect = (e, int) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      client_interests: {
        ...state.client_interests,
        [int._id]: checked
      }
    }))
  }

  const handleObjectivesSelect = (e, obj) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      client_objectives: {
        ...state.client_objectives,
        [obj._id]: checked
      }
    }))
  }

  const onChange = e => setFormData({
    ...formData, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    const interests = Object.keys(formData.client_interests).filter(i => formData.client_interests[i]).map(t => JSON.parse(JSON.stringify({ "tag" : t })))
    const objectives = Object.keys(formData.client_objectives).filter(o => formData.client_objectives[o]).map(o => JSON.parse(JSON.stringify({ "objective" : o })))
    if (interests.length > 0 && objectives.length > 0) {
      createClient({
        ...formData,
        client_interests: interests,
        client_objectives: objectives
      }, history);
    } else {
      setAlert('Seleziona almeno un interesse e un obiettivo', 'danger');
    }
  }

  useEffect(() => {
    getInterests();
    getObjectives();
    getDiets();
  }, [getInterests, getObjectives, getDiets]);
  useEffect(() => {
    setFormData(state => ({
        ...state,
        client_interests: tags.reduce((state, tag) =>
            ({ ...state, [tag._id]: false }),
          {}),
        client_objectives: objectives.reduce((state, obj) =>
            ({ ...state, [obj._id]: false }),
          {})
    }));
  }, [tags, objectives]);

  return (
    <Fragment>
      <h1 className="large text-primary">
        Crea il tuo profilo
      </h1>
      <p className="lead">
        <i className="fas fa-user"> Facci sapere chi sei!</i>
      </p>
      <small>* campo obbligatorio</small>
      <form className="form" onSubmit={e => onSubmit(e)}>
        <div className="form-group">
          <label htmlFor="client_height_create" className="d-none">Inserisci la tua altezza</label>
          <input
            type="number"
            placeholder="* Altezza"
            id="client_height_create"
            name="client_height"
            value={client_height}
            onChange={e => onChange(e)}
            required
          />
          <small className="form-text">
            Potrai modificarlo in futuro se necessario
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="client_objectiveWeight_create" className="d-none">Inserisci il tuo peso desiderato</label>
          <input
            type="number"
            placeholder="* Peso desiderato"
            id="client_objectiveWeight_create"
            name="client_objectiveWeight"
            value={client_objectiveWeight}
            onChange={e => onChange(e)}
            required
          />
          <small className="form-text">
            Potrai aggiornarlo in futuro in base ai tuoi obiettivi
          </small>
        </div>
        <div className="form-group">
          <fieldset>
            <legend>Seleziona i tuoi interessi</legend>
            {
              tags
              .sort()
              .map((tag, key) => {
                const tagCheck = client_interests[tag._id]
                return (
                  <label className="m-1 cb-item" key={key}>
                    {tag.tag_name}
                  <input
                    name={tag.tag_name}
                    type="checkbox"
                    checked={tagCheck ? tagCheck : false}
                    onChange={e => handleInterestsSelect(e, tag)}
                    className="mx-3"
                  /></label>
                );
              })
            }
          </fieldset>
        </div>
        <small className="form-text">
          Potrai aggiungerne/rimuoverne in futuro
        </small>
        <div className="form-group">
        <fieldset>
            <legend>Seleziona i tuoi obiettivi</legend>
            {
              objectives
              .sort()
              .map((obj, key) => {
                const objCheck = client_objectives[obj._id]
                return (
                  <label className="m-1 cb-item" key={key}>
                    {obj.objective_name}
                  <input
                    name={obj.objective_name}
                    type="checkbox"
                    checked={objCheck ? objCheck : false}
                    onChange={e => handleObjectivesSelect(e, obj)}
                    className="mx-3"
                  /></label>
                );
              })
            }
          </fieldset>
        </div>
        <small className="form-text">
          Potrai modificarli in futuro
        </small>
        <div className="form-group">
          <label htmlFor="client_diet_create" className="d-none">Seleziona dieta</label>
          <select id="client_diet_create" name="client_diet" value={client_diet} onChange={e => onChange(e)}>
            <option key="0" value="0" disabled="disabled">Seleziona dieta</option>
            {
              diet.diets.map(function(diet) {
                return (<option key={diet._id} value={diet._id}>{diet.diet_name}</option>);
              })
            }
            <option key="-1" value={undefined}>Nessuna</option>
          </select>
          <small className="form-text">
            Potrai modificarla in futuro
          </small>
        </div>
        <input type='submit' value="Configura profilo" className='btn btn-primary my-1' />
      </form>
    </Fragment>
  );
};

CreateClient.propTypes = {
  getInterests: PropTypes.func.isRequired,
  getObjectives: PropTypes.func.isRequired,
  getDiets: PropTypes.func.isRequired,
  setAlert: PropTypes.func.isRequired,
  createClient: PropTypes.func.isRequired,
  tags: PropTypes.object.isRequired,
  objectives: PropTypes.object.isRequired,
  diet: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  tags: state.tags,
  objectives: state.objectives,
  diet: state.diet
});

export default connect(mapStateToProps, { getInterests, getObjectives, getDiets, setAlert, createClient })(withRouter(CreateClient));
