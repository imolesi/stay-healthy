import React, { Fragment, useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addObjective, getCurrentClient, getClients } from '../../actions/client';
import { getObjectives } from '../../actions/objective';

const EditClientObjectives = ({
  getObjectives,
  addObjective,
  getCurrentClient,
  getClients,
  auth: { user, isAuthenticated },
  history,
  objectives: { objectives },
  client: { client, clients } }) => {

  const [formData, setFormData] = useState({
    client_objectives: {}
  });

  const {
    client_objectives
  } = formData;

  useEffect(() => {
    getClients();
  }, [getClients])
  useEffect(() => {
    if (isAuthenticated && user && clients && clients.find(e => e.client_user && e.client_user._id === user._id)) {
      getCurrentClient();
    }
    getObjectives();
  }, [getObjectives, getCurrentClient, isAuthenticated, user, clients])

  const handleSelect = (e, obj) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      client_objectives: {
        ...state.client_objectives,
        [obj._id]: checked
      }
    }))
  }

  const onSubmit = async e => {
    e.preventDefault();
    for (var key in client_objectives) {
      if (client_objectives[key]) {
        await addObjective({ client_objective: key }, history); // put request per ogni obiettivo
      };
    }
  }

  return (
    <Fragment>
      <p className="lead">
        <i className="fas fa-exclamation"></i> Indica quali sono i tuoi obiettivi
      </p>
      <form className="form" onSubmit={e => onSubmit(e)}>
      {
        client &&
        objectives
        .filter(e => !client.client_objectives.map(f => f.objective).includes(e._id))
        .sort()
        .map((obj, id) => {
          const objCheck = client_objectives[obj._id]
          return (
            <label className="m-1 cb-item" key={id}>
              {obj.objective_name}
            <input
              name={obj.objective_name}
              type="checkbox"
              checked={objCheck ? objCheck : false}
              onChange={e => handleSelect(e, obj)}
              className="mx-3"
            /></label>
          );
        })
      }
        <small className="form-text">
          Gli obiettivi ti permettono di strutturare meglio il tuo benessere
        </small>
        <input type="submit" className="btn btn-primary my-1" value="Aggiungi" />
      </form>
    </Fragment>
  )
}

EditClientObjectives.propTypes = {
  getObjectives: PropTypes.func.isRequired,
  addObjective: PropTypes.func.isRequired,
  getCurrentClient: PropTypes.func.isRequired,
  getClients: PropTypes.func.isRequired,
  objectives: PropTypes.object.isRequired,
  client: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  objectives: state.objectives,
  client: state.client
});

export default connect(mapStateToProps, { getObjectives, addObjective, getCurrentClient, getClients })(withRouter(EditClientObjectives))
