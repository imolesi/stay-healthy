import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import CourseItem from '../courses/course-item.component';
import { getCourse } from '../../actions/course';
import { getExperts } from '../../actions/expert';

const Course = ({ expert: { experts }, getExperts, getCourse, auth, course: { course, courses, loading }, match }) => {
  useEffect(() => {
    getCourse(match.params.id);
    getExperts();
  }, [getCourse, getExperts, match.params.id, courses]);
  const expert = course && experts.find(e => e.expert_user && e.expert_user._id === course.course_trainer)
  return (
    loading ? <Spinner /> : (
      <Fragment>
        { course && expert && (<CourseItem course={course} expert={expert} />) }
      </Fragment>
    )
  );
};

Course.propTypes = {
  getCourse: PropTypes.func.isRequired,
  getExperts: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  expert: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  expert: state.expert,
  course: state.course
});

export default connect(mapStateToProps, { getExperts, getCourse })(Course);
