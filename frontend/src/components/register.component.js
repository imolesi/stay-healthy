import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { setAlert } from '../actions/alert';
import { register } from '../actions/auth';
import PropTypes from 'prop-types';

const Register = ({ setAlert, register, isAuthenticated}) => {

  const [formData, setFormData] = useState({
    user_name: '',
    user_lastname: '',
    user_gender: '',
    user_birthDate: '',
    user_username: '',
    user_email: '',
    user_password: '',
    user_passwordConfirm: ''
  });

  const {
    user_name,
    user_lastname,
    user_gender,
    user_birthDate,
    user_username,
    user_email,
    user_password,
    user_passwordConfirm
  } = formData;

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    if (user_password !== user_passwordConfirm) {
      setAlert('Le password non corrispondono', 'danger');
    } else {
      register({
        user_name,
        user_lastname,
        user_gender,
        user_birthDate,
        user_username,
        user_email,
        user_password
      });
    }
  };

  if (isAuthenticated) {
    return <Redirect to='/dashboard' />;
  }

  return(
    <Fragment>
      <h1 className="large text-dark my-3">Crea un nuovo account StayHealthy</h1>
      <p className="lead">
        <i className="fas fa-user m-2" /> Inserisci i tuoi dati
      </p>
      <form className="form" onSubmit={e => onSubmit(e)}>
        <div className="form-group">
          <label htmlFor="user_name" className="d-none">Inserisci il tuo nome</label>
          <input
            type="text"
            placeholder="Nome"
            id="user_name"
            name="user_name"
            value={user_name}
            onChange={e => onChange(e)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="user_lastname" className="d-none">Inserisci il tuo cognome</label>
          <input
            type="text"
            placeholder="Cognome"
            id="user_lastname"
            name="user_lastname"
            value={user_lastname}
            onChange={e => onChange(e)}
            required
          />
        </div>
        <small className="form-text">Sesso:</small>
        <div className="form-group">
          <label htmlFor="user_gender" className="d-none">Seleziona il tuo sesso</label>
          <select
            id="user_gender"
            name="user_gender"
            value={user_gender}
            onChange={e => onChange(e)}
            required>
            <option key="" value="" disabled hidden>Seleziona sesso</option>
            <option key="male" value="male">Maschio</option>
            <option key="female" value="female">Femmina</option>
            <option key="unknown" value="unknown">Non specificato</option>
          </select>
        </div>
        <small className="form-text">Nato il:</small>
        <div className="form-group">
          <label htmlFor="user_birthDate" className="d-none">Inserisci la tua data di nascita</label>
          <input
            type="date"
            id="user_birthDate"
            name="user_birthDate"
            value={user_birthDate}
            onChange={e => onChange(e)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="user_username" className="d-none">Inserisci il tuo username</label>
          <input
            type="text"
            placeholder="Username"
            id="user_username"
            name="user_username"
            value={user_username}
            onChange={e => onChange(e)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="user_email" className="d-none">Inserisci la tua email</label>
          <input
            type="text"
            placeholder="Indirizzo e-mail"
            id="user_email"
            name="user_email"
            value={user_email}
            onChange={e => onChange(e)}
            required
          />
          <small className="form-text">
            Questa applicazione utilizza Gravatar,
            se alla tua mail è associato un profilo Gravatar
            sarà utilizzato l'avatar corrispondente
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="user_password" className="d-none">Inserisci una password</label>
          <input
            type="password"
            placeholder="Password"
            id="user_password"
            name="user_password"
            value={user_password}
            onChange={e => onChange(e)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="user_passwordConfirm" className="d-none">Conferma la password</label>
          <input
            type="password"
            placeholder="Conferma password"
            id="user_passwordConfirm"
            name="user_passwordConfirm"
            value={user_passwordConfirm}
            onChange={e => onChange(e)}
            required
          />
        </div>
        <input type="submit" value="Registrati" className="btn btn-dark" />
      </form>
      <p className="my-1">
        Hai già un account? <Link to="/login">Accedi</Link>
      </p>
    </Fragment>
  );

};

Register.propTypes = {
  setAlert: PropTypes.func.isRequired,
  register: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { setAlert, register })(Register);
