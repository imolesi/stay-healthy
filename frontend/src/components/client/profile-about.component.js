import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getInterests } from '../../actions/tag';
import { getObjectives } from '../../actions/objective';
import { getDiets } from '../../actions/diet';
import { getAchievements } from '../../actions/achievement';

const ProfileAbout = ({
  getInterests,
  getObjectives,
  getDiets,
  getAchievements,
  tags: { tags },
  objectives: { objectives },
  diet: { diets },
  achievement: { achievements },
  client: {
    client_user,
    client_diet,
    client_interests,
    client_objectives,
    client_achievements
  },
  expert: { experts } }) => {
    useEffect(() => {
      getDiets();
      getInterests();
      getObjectives();
      getAchievements();
    }, [getInterests, getObjectives, getDiets, getAchievements]);

    const diet = client_diet && diets.length > 0 && diets.find(e => e._id === client_diet && e.diet_name)
    const expert = client_user && experts.length > 0 && experts.find(e => e.expert_user && e.expert_user._id === client_user._id)

    return (
      <div className="profile-about container bg-light p-2">
        <h1 className="large text-primary my-3">Informazioni utente</h1>
        <div className="row">
          { expert && expert.expert_bio &&
            <div className="col-12">
              <h2 className="medium my-2">Biografia</h2>
              <span>{ expert.expert_bio }</span>
            </div>
          }
          <div className="col-12 col-md-6">
            <h2 className="medium my-2">Dieta corrente</h2>
            { diet && <> <i className="fa fa-cutlery"></i> <span>{ diet.diet_name }</span> </> }
          </div>
          <div className="col-12 col-md-6">
            <h2 className="medium my-2">Interessi</h2>
            <ul>
              {
                client_interests.slice(0, 4).map(tag => {
                    const t = tags.length > 0 && tags.find(e => e._id === tag.tag && e.tag_name)
                    return <li key={tag._id}>
                      <i className="fas fa-exclamation" /> { t && t.tag_name }
                    </li>
                })
              }
            </ul>
          </div>
          <div className="col-12 col-md-6">
            <h2 className="medium my-2">Obiettivi</h2>
            <ul>
              {
                client_objectives.slice(0, 4).map(objective => {
                  const o = objectives.length > 0 && objectives.find(e => e._id === objective.objective && e.objective_name)
                  return <li key={objective._id}>
                    <i className="fas fa-bullseye" /> { o && o.objective_name }
                  </li>
                })
              }
            </ul>
          </div>
          <div className="col-12 col-md-6">
            <h2 className="medium my-2">Achievement</h2>
            <ul>
              {
                client_achievements.map(achievement => {
                  const a = achievements.length > 0 && achievements.find(e => e._id === achievement.achievement && e.achievement_name)
                  return a && <li key={achievement._id}>
                    {a.achievement_image && <i className={`fas fa-ach-${a.achievement_image.slice(a.achievement_image.lastIndexOf('/') + 1, a.achievement_image.lastIndexOf('.'))}`} />} {a.achievement_name}
                  </li>
                })
              }
            </ul>
          </div>
        </div>
      </div>
    );
  };

ProfileAbout.propTypes = {
  client: PropTypes.object.isRequired,
  getInterests: PropTypes.func.isRequired,
  getObjectives: PropTypes.func.isRequired,
  getAchievements: PropTypes.func.isRequired,
  getDiets: PropTypes.func.isRequired,
  tags: PropTypes.object.isRequired,
  objectives: PropTypes.object.isRequired,
  diet: PropTypes.object.isRequired,
  achievement: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  tags: state.tags,
  objectives: state.objectives,
  diet: state.diet,
  achievement: state.achievement,
  expert: state.expert
});

export default connect(mapStateToProps, { getInterests, getObjectives, getDiets, getAchievements })(ProfileAbout);
