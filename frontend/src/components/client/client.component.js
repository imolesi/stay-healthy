import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import ProfileTop from './profile-top.component';
import ProfileAbout from './profile-about.component';
import { getClients } from '../../actions/client';

const Client = ({ getClients, client: { clients, loading }, auth, match }) => {
  useEffect(() => {
    getClients();
  }, [getClients]);
  const client = match && match.params && clients.find(e => e.client_user && e.client_user._id === match.params.id)
  return <Fragment>
    {!client || loading ? <Spinner /> : <Fragment>
      {auth.isAuthenticated && loading === false &&
        auth.user.id === client.client_user &&
        (<Link to="edit-profile" className="btn btn-dark">Modifica profilo</Link>)}
    </Fragment>}
    {client && (<div className="my-3 profile">
      <div className="row">
        <ProfileTop client={client} />
      </div>
      <div className="row">
        <ProfileAbout client={client} />
      </div>
    </div>)}
  </Fragment>
}

Client.propTypes = {
  getClients: PropTypes.func.isRequired,
  client: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  client: state.client,
  auth: state.auth
})

export default connect(mapStateToProps, { getClients })(Client)
