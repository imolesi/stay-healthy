import React from 'react';
import PropTypes from 'prop-types';

const ProfileTop = ({ client: {
  client_user: { user_name, user_lastname, user_avatar }
} }) => {
  return (
    <div className="profile-top bg-dark p-5">
      <img
        className="round-img my-3"
        src={user_avatar}
        alt=""
      />
      <h1 className="large">{user_name + " " + user_lastname}</h1>
    </div>
  )

};

ProfileTop.propTypes = {
  client: PropTypes.object.isRequired
};

export default ProfileTop;
