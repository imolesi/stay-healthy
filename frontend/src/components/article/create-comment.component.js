import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addComment } from '../../actions/article';

const CreateComment = ({ articleId, addComment }) => {
  const initialState = {
    text: ''
  }

  const [formData, setFormData] = useState(initialState);

  const {
    text
  } = formData;

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    addComment(articleId, formData);
    setFormData(initialState);
  };

  return (
    <Fragment>
      <div className="post-form create-comment my-2">
          <label className="label">Facci sapere cosa ne pensi!</label>
          <form className="form" onSubmit={e => onSubmit(e)}>
            <label htmlFor="comment-text" className="d-none">Scrivi il tuo commento</label>
            <textarea
              id="comment-text"
              name="text"
              cols="100"
              rows="3"
              placeholder="Scrivi il tuo commento"
              value={text}
              onChange={e => onChange(e)}
              required>
            </textarea>
            <input type="submit" className="btn btn-dark my-2" value="Invia" />
          </form>
      </div>
    </Fragment>
  )
}

CreateComment.propTypes = {
  addComment: PropTypes.func.isRequired
}

export default connect(null, { addComment })(CreateComment)
