import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import ArticleItem from '../articles/article-item.component';
import CreateComment from '../article/create-comment.component';
import CommentItem from '../article/comment-item.component';
import { getArticle } from '../../actions/article';
import { getExperts } from '../../actions/expert';

const Article = ({ expert: { experts }, getExperts, getArticle, auth, article: { article, articles, loading }, match }) => {
  useEffect(() => {
    getArticle(match.params.id);
    getExperts();
  }, [getArticle, getExperts, match.params.id, articles]);
  const expert = article && experts.find(e => e.expert_user && e.expert_user._id === article.article_author)
  return (
    loading ? <Spinner /> : (
      <Fragment>
          { article && expert && (<ArticleItem article={article} expert={expert} showViewComments={false} />) }
          { auth.user && article && (<CreateComment articleId={article._id} />) }
          <div>
            <span className="item-title">Commenti</span>
            { article && article.article_comments && article.article_comments.map(comment => (
              <CommentItem key={comment._id} comment={comment} articleId={article._id} />
            ))}
          </div>
      </Fragment>
    )
  );
};

Article.propTypes = {
  getArticle: PropTypes.func.isRequired,
  getExperts: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  expert: PropTypes.object.isRequired,
  article: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  expert: state.expert,
  article: state.article
});

export default connect(mapStateToProps, { getExperts, getArticle })(Article);
