import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import { connect } from 'react-redux';
import { deleteComment } from '../../actions/article'

const CommentItem = ({
  articleId,
  comment: {
    _id,
    text,
    name,
    avatar,
    user,
    date
  },
  auth,
  deleteComment
}) => {
  return (
    <Fragment>
    <div className="comment">
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-6 center">
            <Link to={`/profile/${user}`}>
              <img
                className="comment-image"
                src={avatar}
                alt=""
              />
              <div className="author-name">{name}</div>
            </Link>
          </div>
          <div className="col-12 col-md-6">
            <p className="comment-text">
            {text}
            </p>
            <p className="post-date comment-date"><Moment format="DD/MM/YYYY">{date}</Moment></p>
            {auth.user && !auth.loading && user === auth.user._id && (
              <div className="article-buttons">
                <button onClick={e => deleteComment(articleId, _id)} type="button" className="btn btn-danger">
                  <i className="fas fa-times" />
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
    </Fragment>
  )
}

CommentItem.propTypes = {
  articleId: PropTypes.string.isRequired,
  comment: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  deleteComment: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth
})

export default connect(mapStateToProps, { deleteComment })(CommentItem)
