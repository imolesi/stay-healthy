import React, { Fragment, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { login } from '../actions/auth';

const Login = ({ login, isAuthenticated }) => {

  const [formData, setFormData] = useState({
    user_username: '',
    user_password: ''
  });

  const {
    user_username,
    user_password
  } = formData;

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    login({user_username, user_password});
  };

  if (isAuthenticated) {
    return <Redirect to='/dashboard' />;
  }

  return (
    <Fragment>
    <h1 className="large text-dark my-3">Accedi al tuo account StayHealthy</h1>
    <p className="lead">
      <i className="fas fa-user m-2" />
      Inserisci le credenziali di accesso
    </p>
    <form className="form" onSubmit={e => onSubmit(e)}>
      <div className="form-group">
        <label htmlFor="user_username" className="d-none">Inserisci il tuo username</label>
        <input
          type="text"
          placeholder="Username"
          id="user_username"
          name="user_username"
          value={user_username}
          onChange={e => onChange(e)}
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="user_password" className="d-none">Inserisci la tua password</label>
        <input
          type="password"
          placeholder="Password"
          id="user_password"
          name="user_password"
          value={user_password}
          onChange={e => onChange(e)}
          minLength="6"
          required
        />
      </div>
      <input type="submit" value="Accedi" className="btn btn-dark" />
    </form>
    <p className="my-1">
      Non hai un account? <Link to="/register">Registrati</Link>
    </p>
    </Fragment>
  );

};

Login.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { login })(Login);
