import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import ClientItem from './client-item.component';
import { getClients } from '../../actions/client';

const Clients = ({ getClients, client: { clients, loading } }) => {
  useEffect(() => {
    getClients();
  }, [getClients]);
  return <Fragment>
    { loading ? <Spinner /> : <Fragment>
      <h1 className="large text-primary">Utenti</h1>
      <p className="lead">
        <i className="fas fa-user-circle"></i> Consulta i profili degli altri utenti
      </p>
      <div>
        {clients.length > 0 ?
          clients.map(client => (
           client.client_user && <ClientItem key={client._id} client={client} />
        )) : <span>Nessun utente trovato</span> }
      </div>
      </Fragment>}
    </Fragment>
}

Clients.propTypes = {
  getClients: PropTypes.func.isRequired,
  client: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  client: state.client
});

export default connect(mapStateToProps, { getClients })(Clients);
