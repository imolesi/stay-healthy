import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import { getCourses } from '../../actions/course';
import Moment from 'react-moment';

const ManageCourses = ({ getCourses, course: { courses, loading }, auth }) => {
  useEffect(() => {
    getCourses();
  }, [getCourses]);
  return (
    loading ? <Spinner /> : (
      <Fragment>
        <h1 className="large text-primary my-3">Gestisci corsi e attività</h1>
        <div>
          <Link to={`/create-course`} className="btn btn-dark btn-lg my-2">
            Crea nuova attività o corso
          </Link>
          <p className="my-3 area-title">Gestisci corsi e attività creati</p>
          <div className="center">
            <div className="container table">
            {
                courses && courses.length > 0 ?
                courses.sort((a1, a2) => a1.course_publicationDate - a2.course_publicationDate)
                .filter(c => auth && auth.user && c.course_trainer === auth.user._id)
                .map((course, key) =>
                    <div className="row" key={key}>
                      <div className="col-12 col-md-9"><Link key={key} to={`/edit-course/${course._id}`} className="">{course.course_name}</Link></div>
                      <div className="col-12 col-md-3"><Moment format="DD/MM/YYYY">{course.course_publicationDate}</Moment></div>
                    </div>
                ) : <div className="row">Nessun corso</div>
            }
            </div>
          </div>
        </div>
      </Fragment>
    )
  );
};

ManageCourses.propTypes = {
  getCourses: PropTypes.func.isRequired,
  course: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  course: state.course,
  auth: state.auth
});

export default connect(mapStateToProps, { getCourses })(ManageCourses);
