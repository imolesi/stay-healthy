import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import CourseItem from './course-item.component';
import { getCourses } from '../../actions/course';
import { getExperts } from '../../actions/expert';

const Courses = ({ getCourses, getExperts, course: { courses, loading }, expert: { experts } }) => {
  useEffect(() => {
    getExperts();
    getCourses();
  }, [getExperts, getCourses]);
  return (
    loading ? <Spinner /> : (
      <Fragment>
        <h1 className="large text-primary">Corsi e attività</h1>
        <div>
          {
            courses && courses.map(function(course) {
              const exp = experts.find(e => e.expert_user._id === course.course_trainer);
              return exp && (<CourseItem key={course._id} course={course} expert={exp} />)
            })
          }
        </div>
      </Fragment>
    )
  );
};

Courses.propTypes = {
  getCourses: PropTypes.func.isRequired,
  getExperts: PropTypes.func.isRequired,
  course: PropTypes.object.isRequired,
  expert: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  course: state.course,
  expert: state.expert
});

export default connect(mapStateToProps, { getExperts, getCourses })(Courses);
