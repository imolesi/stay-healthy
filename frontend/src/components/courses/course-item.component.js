import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import Moment from 'react-moment';
import { connect } from 'react-redux';
import { subscribe, unsubscribe, deleteCourse } from '../../actions/course';
import { getObjectives } from '../../actions/objective';

const DaysEnum = Object.freeze({"Lunedì":1, "Martedì":2, "Mercoledì":3, "Giovedì":4, "Venerdì":5, "Sabato":6, "Domenica":7});

const CourseItem = ({
  subscribe,
  unsubscribe,
  deleteCourse,
  getObjectives,
  auth,
  expert,
  course: {
    _id,
    course_name,
    course_description,
    course_trainer,
    course_avatar,
    course_publicationDate,
    course_startDate,
    course_endDate,
    course_days,
    course_subscribers,
    course_objectives
  },
  objectives: { objectives },
  history
 }) => {
  useEffect(() => {
    getObjectives();
  }, [getObjectives])

  const isUserSubscribed = subscribers => {
    if (subscribers.filter(sub => auth && auth.user && sub.subscriber === auth.user._id).length > 0) {
      return true;
    } else {
      return false;
    }
  }

  const canSubscribe = () => {
    // current date before beginning of next day to course end date
    const subscribe_expiree = new Date(new Date(course_endDate).toISOString().substr(0, 10))
    subscribe_expiree.setDate(subscribe_expiree.getDate() + 1)
    return new Date() < subscribe_expiree
  }

  return (
    <div className="post">
      <div className="item">
        <Link to={`/courses/${_id}`}>
        <span className="item-title">{course_name}</span>
        </Link>
        <p className="post-date">
          Pubblicato il <Moment format="DD/MM/YYYY">{course_publicationDate}</Moment>
        </p>
        <span className="my-1" dangerouslySetInnerHTML={{__html: course_description}} />
        <div className="my-1">Giorni fissi:
          {
            course_days && Object.entries(DaysEnum).sort((d1, d2) => d1[1] - d2[1])
            .filter(d => course_days.includes(d[1]))
            .map((d, key) => <div key={key} className="pr-3">{d[0]}</div>)
          }
        </div>
        <div>Data di inizio <Moment format="DD/MM/YYYY">{course_startDate}</Moment></div>
        <div>Data di fine <Moment format="DD/MM/YYYY">{course_endDate}</Moment></div>
        <div className="mt-1">
          {
            objectives && course_objectives && objectives.filter(o => course_objectives.map(o2 => o2.objective).includes(o._id))
            .map((o, key) => <span key={key} className="badge badge-info mx-1">{o.objective_name}</span>)
          }
        </div>
        <div className="author bg-light">
          <Link to={`/profile/${course_trainer}`}>
            <img
              className="author-image"
              src={course_avatar}
              alt=""
            />
            <div className="author-name">di {expert && (expert.expert_user.user_name + ' ' + expert.expert_user.user_lastname)}</div>
            <div className="author-bio">{expert.expert_bio}</div>
          </Link>
        </div>
        <div className="article-buttons">
        <button onClick={e => subscribe(_id)} type="button" className="btn btn-light" disabled={!auth.isAuthenticated || isUserSubscribed(course_subscribers) || !canSubscribe()}>
          Iscriviti
        </button>
        <button onClick={e => unsubscribe(_id)} type="button" className="btn btn-light" disabled={!auth.isAuthenticated || !isUserSubscribed(course_subscribers)}>
          Disiscriviti
        </button>
        { auth && auth.user && auth.isAuthenticated && !auth.loading && course_trainer === auth.user._id && (
          <button onClick={e => deleteCourse(_id, history)} type="button" className="btn btn-danger">
            <i className="fas fa-times"></i>
          </button>
        )}
        </div>
      </div>
    </div>
  )
}

CourseItem.propTypes = {
  expert: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired,
  objectives: PropTypes.object.isRequired,
  subscribe: PropTypes.func.isRequired,
  unsubscribe: PropTypes.func.isRequired,
  deleteCourse: PropTypes.func.isRequired,
  getObjectives: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  objectives: state.objectives
})

export default connect(mapStateToProps, { subscribe, unsubscribe, deleteCourse, getObjectives })(withRouter(CourseItem))
