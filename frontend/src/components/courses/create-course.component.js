import React, { Fragment, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import { addCourse, updateCourse, deleteCourse, getCourse, clearCourse } from '../../actions/course';
import { getObjectives } from '../../actions/objective';
import { setAlert } from '../../actions/alert';
import { Editor } from '@tinymce/tinymce-react';

const DaysEnum = Object.freeze({"Lunedì":1, "Martedì":2, "Mercoledì":3, "Giovedì":4, "Venerdì":5, "Sabato":6, "Domenica":0});

const CreateCourse = ({ addCourse, updateCourse, deleteCourse, getCourse, clearCourse, getObjectives, setAlert, auth: { isAuthenticated }, expert: { expert }, course: { course, loading }, objectives: { objectives }, match, history }) => {

  const initialState = {
    course_name: '',
    course_description: '',
    course_objectives: [],
    course_startDate: new Date(),
    course_endDate: new Date(),
    course_days: []
  }

  const [formData, setFormData] = useState(initialState);

  const {
    course_name,
    course_description,
    course_objectives,
    course_startDate,
    course_endDate,
    course_days
  } = formData;

  const formatDate = dateString => {
    return new Date(dateString).toISOString().substr(0, 10)
  }

  const handleObjectivesSelect = (e, objective) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      course_objectives: {
        ...state.course_objectives,
        [objective._id]: checked
      }
    }))
  }

  const handleDaysSelect = (e, day) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      course_days: {
        ...state.course_days,
        [day[0]]: checked
      }
    }))
  }

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    if (!course_description) {
      setAlert('Scrivi una descrizione', 'danger');
    } else {
      const newObjectives = Object.keys(formData.course_objectives).filter(o => formData.course_objectives[o]).map(o => JSON.parse(JSON.stringify({ "objective" : o })))
      if (newObjectives.length > 0) {
        const newDays = Object.keys(formData.course_days).filter(d => formData.course_days[d]).map(d => DaysEnum[d])
        const submittingForm = {
          ...formData,
          course_objectives: newObjectives
        }
        if (newDays.length > 0) {
          submittingForm.course_days = newDays;
        } else {
          delete submittingForm.course_days;
        }
        if (match && match.params && match.params.id) {
          updateCourse(match.params.id, submittingForm, history);
        } else {
          addCourse(submittingForm, history);
        }
      } else {
        setAlert('Seleziona almeno un obiettivo', 'danger');
      }
    }
  };

  const handleEditorChange = e => {
    setFormData({ ...formData, course_description: e });
  }

  useEffect(() => {
    setFormData({
      course_name: course ? course.course_name : '',
      course_description: course ? course.course_description : '',
      course_objectives: objectives.reduce((state, objective) =>
            ({ ...state, [objective._id]: course && course.course_objectives ? course.course_objectives.map(o => o.objective).includes(objective._id) : false }),
        {}
      ),
      course_startDate: course ? course.course_startDate : new Date(),
      course_endDate: course ? course.course_endDate : new Date(),
      course_days: Object.entries(DaysEnum).reduce((state, day) =>
      ({ ...state, [day[0]]: course && course.course_days ? course.course_days.includes(day[1]) : false}),
        {}
      ),
    });
  }, [course, objectives]);
  useEffect(() => {
    getObjectives();
    if (match && match.params && match.params.id) {
      getCourse(match.params.id);
    } else {
      clearCourse();
    }
  }, [getObjectives, getCourse, clearCourse, match])

  return (
    (!isAuthenticated || loading) ? <Spinner /> : (<Fragment>
      <div className="post-form">
          <h1 className="large text-primary">{match && match.params && match.params.id && course && course_name ? "Modifica \"" + course_name + "\"" : "Crea il tuo corso"}</h1>
        <form className="form my-1" onSubmit={e => onSubmit(e)}>
          <div className="form-group">
            <label htmlFor="course_name" className="d-none">Scrivi il nome del corso</label>
            <textarea
            id="course_name"
            name="course_name"
            cols="30"
            rows="1"
            placeholder="Titolo"
            value={course_name}
            onChange={e => onChange(e)}
            required>
            </textarea>
          </div>
          <div className="form-group">
          <Editor
              initialValue={course && course.course_description && "<p>"+course.course_description+"</p>"}
              apiKey="n6y968kozaobofk1ue36r8kjd5ubagvgcnjf0j8fr045pur5"
              init={{
                height: 500,
                menubar: false,
                plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table paste code help wordcount'
                ],
                toolbar:
                  'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                placeholder: 'Descrizione corso'
                }}
              onEditorChange={e => handleEditorChange(e)}
            />
          </div>
          <div className="form-group">
            <label className="m-3">
              <span>Data di inizio</span>
              <input
                type="date"
                name="course_startDate"
                value={formatDate(course_startDate)}
                max={formatDate(course_endDate)}
                onChange={e => onChange(e)}
                required
              />
            </label>
          </div>
          <div className="form-group">
            <label className="m-3">
              <span>Data di fine</span>
              <input
                type="date"
                name="course_endDate"
                value={formatDate(course_endDate)}
                min={formatDate(course_startDate)}
                onChange={e => onChange(e)}
                required
              />
            </label>
          </div>
          <div className="form-group">
            {
              Object.entries(course_days)
              .sort((d1, d2) => d1[0] - d2[0])
              .map((day, key) => {
                const dayCheck = course_days[day[0]]
                return (
                  <label className="m-1 cb-item" key={key}>
                    {day[0]}
                  <input
                    name={day[0]}
                    type="checkbox"
                    checked={dayCheck ? dayCheck : false}
                    onChange={e => handleDaysSelect(e, day)}
                    className="mx-3"
                  /></label>
                );
              })
            }
          </div>
          <div className="form-group">
            <fieldset>
              <legend>Seleziona gli obiettivi a cui si mira</legend>
              {
                objectives
                .sort()
                .map((objective, id) => {
                  const objectiveCheck = course_objectives[objective._id]
                  return (
                    <label className="m-1 cb-item" key={id}>
                      {objective.objective_name}
                    <input
                      name={objective.objective_name}
                      type="checkbox"
                      checked={objectiveCheck ? objectiveCheck : false}
                      onChange={e => handleObjectivesSelect(e, objective)}
                      disabled={expert && expert.expert_objectives && !expert.expert_objectives.filter(k => k.objective).map(k => k.objective).includes(objective._id)}
                      className="mx-3"
                    /></label>
                  );
                })
              }
            </fieldset>
          </div>
          {match && match.params && match.params.id && (<div className="my-2">
              <button type="button" className="btn btn-danger" onClick={() => deleteCourse(match.params.id, history)}>
                <i className="fas"></i> Elimina questo corso
              </button>
            </div>)}
          <input type="submit" className="btn btn-dark my-1" value={(match && match.params && match.params.id) ? "Aggiorna" : "Crea"} />
        </form>
      </div>
    </Fragment>)
  )
}

CreateCourse.propTypes = {
  auth: PropTypes.object.isRequired,
  expert: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired,
  objectives: PropTypes.object.isRequired,
  addCourse: PropTypes.func.isRequired,
  updateCourse: PropTypes.func.isRequired,
  deleteCourse: PropTypes.func.isRequired,
  getCourse: PropTypes.func.isRequired,
  clearCourse: PropTypes.func.isRequired,
  getObjectives: PropTypes.func.isRequired,
  setAlert: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  expert: state.expert,
  course: state.course,
  objectives: state.objectives
})

export default connect(mapStateToProps, { addCourse, updateCourse, deleteCourse, getCourse, clearCourse, getObjectives, setAlert })(withRouter(CreateCourse))
