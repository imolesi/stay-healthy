import React, { Fragment, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import { createExpert, getCurrentExpert, getExperts, deleteExpert } from '../../actions/expert';
import { getInterests } from '../../actions/tag';
import { getObjectives } from '../../actions/objective';
import { setAlert } from '../../actions/alert';

const EditExpert = ({ getCurrentExpert, getExperts, createExpert, getInterests, getObjectives, deleteExpert, setAlert, expert: { expert, experts, loading }, auth: { user, isAuthenticated }, tags: { tags }, objectives: { objectives }, history }) => {

  const [formData, setFormData] = useState({
    expert_bio: '',
    expert_knowledge: [],
    expert_objectives: []
  });

  useEffect(() => {
    getExperts();
    getInterests();
    getObjectives();
  }, [getExperts, getInterests, getObjectives])
  useEffect(() => {
    if (isAuthenticated && user && experts && experts.find(e => e.expert_user && e.expert_user._id === user._id)) {
      getCurrentExpert();
    }
  }, [getCurrentExpert, isAuthenticated, user, experts]);
  useEffect(() => {
    setFormData(state => ({
        ...state,
        expert_bio: expert && expert.expert_bio ? expert.expert_bio : '',
        expert_knowledge: tags.reduce((state, tag) =>
            ({ ...state, [tag._id]: expert && expert.expert_knowledge ? expert.expert_knowledge.map(t => t.tag).includes(tag._id) : false }),
          {}),
        expert_objectives: objectives.reduce((state, obj) =>
            ({ ...state, [obj._id]: expert && expert.expert_objectives ? expert.expert_objectives.map(o => o.objective).includes(obj._id) : false }),
          {})
    }));
  }, [expert, tags, objectives]);

  const {
    expert_bio,
    expert_knowledge,
    expert_objectives
  } = formData;

  const handleInterestsSelect = (e, int) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      expert_knowledge: {
        ...state.expert_knowledge,
        [int._id]: checked
      }
    }))
  }

  const handleObjectivesSelect = (e, obj) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      expert_objectives: {
        ...state.expert_objectives,
        [obj._id]: checked
      }
    }))
  }

  const onChange = e => setFormData({
    ...formData, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    const interests = Object.keys(formData.expert_knowledge).filter(i => formData.expert_knowledge[i]).map(t => JSON.parse(JSON.stringify({ "tag" : t })))
    const objectives = Object.keys(formData.expert_objectives).filter(o => formData.expert_objectives[o]).map(o => JSON.parse(JSON.stringify({ "objective" : o })))
    if (interests.length > 0 && objectives.length > 0) {
        createExpert({
          ...formData,
          expert_knowledge: interests,
          expert_objectives: objectives
        }, history, true);
      } else {
        setAlert('Seleziona almeno un interesse e un obiettivo', 'danger');
      }
  }

  return (
    loading ? <Spinner /> : (<Fragment>
      <p className="lead">
        <i className="fas fa-user"> Modifica la tua situazione corrente</i>
      </p>
      <small>* campo obbligatorio</small>
      <form className="form" onSubmit={e => onSubmit(e)}>
        <div className="form-group">
          <label htmlFor="expert_bio_edit" className="d-none">Modifica biografia</label>
          <textarea
            placeholder="* Biografia"
            id="expert_bio_edit"
            name="expert_bio"
            cols="100"
            rows="3"
            value={expert_bio}
            onChange={e => onChange(e)}
            required
          />
          <small className="form-text">
            La tua biografia
          </small>
        </div>
        <small className="form-text">
          Potrai modificarla in futuro
        </small>
        <div className="form-group">
          <fieldset>
            <legend>Aggiorna le tue conoscenze</legend>
            {
              tags
              .sort()
              .map((tag, key) => {
                const tagCheck = expert_knowledge[tag._id]
                return (
                  <label className="m-1 cb-item" key={key}>
                    {tag.tag_name}
                  <input
                    name={tag.tag_name}
                    type="checkbox"
                    checked={tagCheck ? tagCheck : false}
                    onChange={e => handleInterestsSelect(e, tag)}
                    className="mx-3"
                  /></label>
                );
              })
            }
          </fieldset>
        </div>
        <small className="form-text">
          Potrai aggiungerne/rimuoverne in futuro
        </small>
        <div className="form-group">
        <fieldset>
            <legend>Aggiorna gli obiettivi di tua competenza</legend>
            {
              objectives
              .sort()
              .map((obj, key) => {
                const objCheck = expert_objectives[obj._id]
                return (
                  <label className="m-1 cb-item" key={key}>
                    {obj.objective_name}
                  <input
                    name={obj.objective_name}
                    type="checkbox"
                    checked={objCheck ? objCheck : false}
                    onChange={e => handleObjectivesSelect(e, obj)}
                    className="mx-3"
                  /></label>
                );
              })
            }
          </fieldset>
        </div>
        <small className="form-text">
          Potrai modificarli in futuro
        </small>
        <input type='submit' value="Aggiorna" className='btn btn-primary my-1' />
      </form>
    </Fragment>)
  );
};

EditExpert.propTypes = {
  getCurrentExpert: PropTypes.func.isRequired,
  getExperts: PropTypes.func.isRequired,
  createExpert: PropTypes.func.isRequired,
  getInterests: PropTypes.func.isRequired,
  getObjectives: PropTypes.func.isRequired,
  deleteExpert: PropTypes.func.isRequired,
  setAlert: PropTypes.func.isRequired,
  expert: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  tags: PropTypes.object.isRequired,
  objectives: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  expert: state.expert,
  tags: state.tags,
  objectives: state.objectives
});

export default connect(mapStateToProps, { getExperts, getCurrentExpert, createExpert, getInterests, getObjectives, deleteExpert, setAlert })(withRouter(EditExpert));
