import React, { Fragment, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createExpert } from '../../actions/expert';
import { getInterests } from '../../actions/tag';
import { getObjectives } from '../../actions/objective';
import { setAlert } from '../../actions/alert';

const CreateExpert = ({ getInterests, getObjectives, setAlert, createExpert, tags: { tags }, objectives: { objectives }, history }) => {
  const [formData, setFormData] = useState({
    expert_bio: '',
    expert_knowledge: [],
    expert_objectives: []
  });

  const {
    expert_bio,
    expert_knowledge,
    expert_objectives
  } = formData;

  const handleInterestsSelect = (e, int) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      expert_knowledge: {
        ...state.expert_knowledge,
        [int._id]: checked
      }
    }))
  }

  const handleObjectivesSelect = (e, obj) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      expert_objectives: {
        ...state.expert_objectives,
        [obj._id]: checked
      }
    }))
  }

  const onChange = e => setFormData({
    ...formData, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    const interests = Object.keys(formData.expert_knowledge).filter(i => formData.expert_knowledge[i]).map(t => JSON.parse(JSON.stringify({ "tag" : t })))
    const objectives = Object.keys(formData.expert_objectives).filter(o => formData.expert_objectives[o]).map(o => JSON.parse(JSON.stringify({ "objective" : o })))
    if (interests.length > 0 && objectives.length > 0) {
      createExpert({
        ...formData,
        expert_knowledge: interests,
        expert_objectives: objectives
      }, history);
    } else {
      setAlert('Seleziona almeno un interesse e un obiettivo', 'danger');
    }
  }

  useEffect(() => {
    getInterests();
    getObjectives();
  }, [getInterests, getObjectives]);
  useEffect(() => {
    setFormData(state => ({
        ...state,
        expert_knowledge: tags.reduce((state, tag) =>
            ({ ...state, [tag._id]: false }),
          {}),
        expert_objectives: objectives.reduce((state, obj) =>
            ({ ...state, [obj._id]: false }),
          {})
    }));
  }, [tags, objectives]);

  return (
    <Fragment>
      <h1 className="large text-primary">
        Crea il tuo profilo di esperto
      </h1>
      <p className="lead">
        <i className="fas fa-user">Rendi pubbliche le tue competenze!</i>
      </p>
      <small>* campo obbligatorio</small>
      <form className="form" onSubmit={e => onSubmit(e)}>
        <div className="form-group">
          <label htmlFor="expert_bio_create" className="d-none">Modifica biografia</label>
          <textarea
            placeholder="* Biografia"
            id="expert_bio_create"
            name="expert_bio"
            cols="100"
            rows="3"
            value={expert_bio}
            onChange={e => onChange(e)}
            required
          />
          <small className="form-text">
            Potrai modificarlo in futuro se necessario
          </small>
        </div>
        <div className="form-group">
          <fieldset>
            <legend>Seleziona le tue conoscenze</legend>
            {
              tags
              .sort()
              .map((tag, key) => {
                const tagCheck = expert_knowledge[tag._id]
                return (
                  <label className="m-1 cb-item" key={key}>
                    {tag.tag_name}
                  <input
                    name={tag.tag_name}
                    type="checkbox"
                    checked={tagCheck ? tagCheck : false}
                    onChange={e => handleInterestsSelect(e, tag)}
                    className="mx-3"
                  /></label>
                );
              })
            }
          </fieldset>
        </div>
        <small className="form-text">
          Potrai aggiungerne/rimuoverne in futuro
        </small>
        <div className="form-group">
        <fieldset>
            <legend>Seleziona gli obiettivi di tua competenza</legend>
            {
              objectives
              .sort()
              .map((obj, key) => {
                const objCheck = expert_objectives[obj._id]
                return (
                  <label className="m-1 cb-item" key={key}>
                    {obj.objective_name}
                  <input
                    name={obj.objective_name}
                    type="checkbox"
                    checked={objCheck ? objCheck : false}
                    onChange={e => handleObjectivesSelect(e, obj)}
                    className="mx-3"
                  /></label>
                );
              })
            }
          </fieldset>
        </div>
        <small className="form-text">
          Potrai modificarli in futuro
        </small>
        <input type='submit' value="Crea" className='btn btn-primary my-1' />
      </form>
    </Fragment>
  );
};

CreateExpert.propTypes = {
  getInterests: PropTypes.func.isRequired,
  getObjectives: PropTypes.func.isRequired,
  setAlert: PropTypes.func.isRequired,
  createExpert: PropTypes.func.isRequired,
  tags: PropTypes.object.isRequired,
  objectives: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  tags: state.tags,
  objectives: state.objectives
});

export default connect(mapStateToProps, { getInterests, getObjectives, setAlert, createExpert })(withRouter(CreateExpert));
