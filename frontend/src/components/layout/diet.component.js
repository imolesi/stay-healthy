import React from 'react';
import PropTypes from 'prop-types'

import CanvasJSReact from '../../lib/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const Diet = ({ diet }) => {

  const options = {
			animationEnabled: true,
			title: {
				text: "Valori nutrizionali"
			},
			data: [{
				type: "pie",
				startAngle: 75,
				toolTipContent: "<b>{label}</b>: {y}%",
				indexLabelFontSize: 16,
				indexLabel: "{label} - {y}%",
				dataPoints: []
			}]
	}

  var proteins = 0;
  var carbs = 0;
  var fats = 0;
  var minerals = 0;
  var vitamins = 0;

  diet.diet_meals.forEach(m => m.intake.filter(i => i.nutrient === "Proteine").forEach(i => proteins += i.quantity));
  diet.diet_meals.forEach(m => m.intake.filter(i => i.nutrient === "Carboidrati").forEach(i => carbs += i.quantity));
  diet.diet_meals.forEach(m => m.intake.filter(i => i.nutrient === "Grassi").forEach(i => fats += i.quantity));
  diet.diet_meals.forEach(m => m.intake.filter(i => i.nutrient === "Sali minerali").forEach(i => minerals += i.quantity));
  diet.diet_meals.forEach(m => m.intake.filter(i => i.nutrient === "Vitamine").forEach(i => vitamins += i.quantity));

  var totalNutrients = proteins + carbs + fats + minerals + vitamins;

  options.data[0].dataPoints.push({ y: (100 / totalNutrients * proteins).toFixed(2), label: "Proteine" });
  options.data[0].dataPoints.push({ y: (100 / totalNutrients * carbs).toFixed(2), label: "Carboidrati" });
  options.data[0].dataPoints.push({ y: (100 / totalNutrients * fats).toFixed(2), label: "Grassi" });
  options.data[0].dataPoints.push({ y: (100 / totalNutrients * minerals).toFixed(2), label: "Sali minerali" });
  options.data[0].dataPoints.push({ y: (100 / totalNutrients * vitamins).toFixed(2), label: "Vitamine" });

  return(
    <div>
      <p>
        {
          diet && diet.diet_description
        }
      </p>
      <CanvasJSChart options={options} />
    </div>
  );
};

Diet.propTypes = {
  diet: PropTypes.object.isRequired
}

export default Diet;
