import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

const Alert = ({ alerts }) =>
  alerts !== null &&
  alerts.length > 0 &&
  <div className='container fixed-top'>
  {alerts.map((alert, key) => (
    <div key={key} className='row'>
      <div key={alert.id} className={`col-xs-12 col-sm-6 col-sm-offset-3 c-alert alert alert-${alert.alertType}`}>
        { alert.msg }
      </div>
    </div>
  ))}
  </div>

Alert.propTypes = {
  alerts: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  alerts: state.alert
});

export default connect(mapStateToProps)(Alert)
