import React from 'react';

const Footer = () => {

    return <footer className="bg-light text-center text-lg-start">
    <div className="container p-4">
      <div className="row">
        <div className="col-12 col-md-6">
          <span className="text-uppercase">Link</span>
  
          <ul className="list-unstyled">
            <li>
              <a href="/policy#terms">Termini e condizioni d'uso</a>
            </li>
            <li>
              <a href="/policy#privacy">Informativa sulla privacy</a>
            </li>
            <li>
              <a href="/policy#cookies">Informativa sui cookie</a>
            </li>
          </ul>
        </div>

        <div className="col-12 col-md-6">
          <span className="text-uppercase">Contatti</span>
  
          <ul className="list-unstyled">
            <li>
              <p>Hai bisogno di aiuto o informazioni? Scrivici a <a href="mailto:infopoint.stayhealthy@gmail.com?subject=Richiesta%20informazioni">infopoint.stayhealthy@gmail.com</a></p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  
    <div className="text-center p-3">
        © 2020 StayHealthy S.p.A. - Via della Salute, 9 - Imola (BO) 40026 - P.IVA 10293847566 - riproduzione riservata
    </div>
  </footer>
}

export default Footer;