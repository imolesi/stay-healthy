import React, { Fragment } from 'react';

const NotFound = () => {
  return (
    <Fragment>
      <h1 className="x-large text-primary">
        <i className="fas fa-exclamation-triangle"></i> Non trovato
      </h1>
      <p className="large">Questa pagina non esiste o è stata rimossa</p>
    </Fragment>
  )
}

export default NotFound
