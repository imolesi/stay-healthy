import React, { Fragment } from 'react';
import spinner from './spinner.gif';

const spinnerComponent = () => (
  <Fragment>
    <img
      src={spinner}
      style= {{width: '600px', height: '300px', margin: 'auto', display: 'block'}}
      alt='Caricamento in corso...'
    />
  </Fragment>
);

export default spinnerComponent;
