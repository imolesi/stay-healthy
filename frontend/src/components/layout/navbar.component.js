import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getExperts, getCurrentExpert } from '../../actions/expert';
import { logout } from '../../actions/auth';

import logo from "../../logo.png";

const Navbar = ({
  getExperts,
  getCurrentExpert,
  auth: {
    isAuthenticated,
    user
  },
  expert: {
    expert,
    experts
  },
  logout
}) => {

  useEffect(() => {
    getExperts();
  }, [getExperts]);

  useEffect(() => {
    if (isAuthenticated && user && experts && experts.find(e => e.expert_user && e.expert_user._id === user._id)) {
      getCurrentExpert();
    }
  }, [getCurrentExpert, isAuthenticated, user, experts]);

  const makeResponsive = async e => {
    let x = document.getElementById("myTopnav");

    if (x.className.includes("topnav responsive")) {
      x.className =  x.className.replace("topnav responsive", "topnav");
    } else if (x.className.includes("topnav")) {
      x.className = x.className.replace("topnav", "topnav responsive");
    }
  };

  const authLinks = (
    <>
    <Link to="/dashboard" onClick={makeResponsive} className="nav-link">
      <i className="fas fa-user" />{' '}
      <span className="hide-sm">Area Personale</span>
    </Link>
    <a onClick={logout} href='#!'className="nav-link">
      <i className="fas fa-sign-out-alt" />{' '}
      <span className="hide-sm">Disconnetti</span>
    </a>
    </>
  );

  const guestLinks = (
    <>
    <Link to="/login" onClick={makeResponsive} className="nav-link">Accedi</Link>
    <Link to="/register" onClick={makeResponsive} className="nav-link">Registrati</Link>
    </>
  );

  const userLinks = (
    <>
    <Link to="/courses" onClick={makeResponsive} className="nav-link">Attività e corsi</Link>
    <Link to="#" onClick={e => showOverlay(e)} className="nav-link">Seguici</Link>
    <Link to="/profiles" onClick={makeResponsive} className="nav-link">Utenti</Link>
    </>
  );
  const expertLinks = (
    <>
    <Link to="/manage-articles" onClick={makeResponsive} className="nav-link">Gestisci articoli</Link>
    <Link to="/manage-courses" onClick={makeResponsive} className="nav-link">Gestisci corsi e attività</Link>
    </>
  );

  const showOverlay = async e => {
    document.getElementById("follow").style.display = "block";
  };

  const hideOverlay = async e => {
    document.getElementById("follow").style.display = "none";
  };

  return (
    <>
    <div className="topnav mb-3" id="myTopnav">
      <Link to="/articles">
        <span className="navbar-brand mx-2"><img src={logo} width="30" height="30" alt="" /></span>StayHealthy
      </Link>
      <button className="navbar-toggler icon" onClick={e => makeResponsive(e)}>
        <i className="fa fa-bars"></i>
      </button>
      {userLinks}
      { isAuthenticated && expert && expertLinks }
      { isAuthenticated ? authLinks : guestLinks }
    </div>
    <div id="follow" className="follow" onClick={e => hideOverlay(e)}>
      <div className="follow-links">
        <h1>Seguici sui nostri Social!</h1>
        <a href="https://www.facebook.com/unibo.it/" className="fab fa-facebook-square fa-5x m-1" title="Facebook"> </a>
        <a href="https://twitter.com/UniboMagazine?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" className="fab fa-twitter-square fa-5x m-1" title="Twitter"> </a>
        <a href="https://www.youtube.com/user/UniBologna" className="fab fa-youtube-square fa-5x m-1" title="YouTube"> </a>
        <a href="https://www.instagram.com/unibo/?hl=it" className="fab fa-instagram-square fa-5x m-1" title="Instagram"> </a>
      </div>
    </div>
    </>
  );
};

Navbar.propTypes = {
  getExperts: PropTypes.func.isRequired,
  getCurrentExpert: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  expert: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  expert: state.expert
});

export default connect(mapStateToProps, { getExperts, getCurrentExpert, logout })(Navbar);
