import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Register from "./../register.component";
import Login from "./../login.component";
import CreateClient from "./../client-forms/create-client.component";
import CreateExpert from "./../expert-forms/create-expert.component";
import Alert from "./../layout/alert.component";
import Policy from "./../layout/policy.component";
import Dashboard from "./../dashboard/dashboard.component";
import Clients from "./../clients/clients.component";
import Client from "./../client/client.component";
import Articles from "./../articles/articles.component";
import Article from "./../article/article.component";
import Courses from "./../courses/courses.component";
import Course from "./../course/course.component";
import ManageArticles from "./../articles/manage-articles.component";
import CreateArticle from "./../articles/create-article.component";
import ManageCourses from "./../courses/manage-courses.component";
import CreateCourse from "./../courses/create-course.component";
import PrivateRoute from "./../routing/privateRoute.component";
import ExpertRoute from "./../routing/expertRoute.component";
import NotFound from "./../layout/not-found.component";

const Routes = () => {
  return (
    <section className='container'>
      <Alert />
      <Switch>
        <Route exact path="/" render={(props) => <Redirect to="/articles" />} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/profiles" component={Clients} />
        <Route exact path="/profile/:id" component={Client} />
        <PrivateRoute exact path="/dashboard" component={Dashboard} />
        <PrivateRoute exact path="/create-profile" component={CreateClient} />
        <PrivateRoute exact path="/create-expert" component={CreateExpert} />
        <Route exact path="/articles" component={Articles} />
        <Route exact path="/articles/:id" component={Article} />
        <Route exact path="/courses" component={Courses} />
        <Route exact path="/courses/:id" component={Course} />
        <ExpertRoute exact path="/manage-articles" component={ManageArticles} />
        <ExpertRoute exact path="/create-article" component={CreateArticle} />
        <ExpertRoute exact path="/edit-article/:id" component={CreateArticle} />
        <ExpertRoute exact path="/manage-courses" component={ManageCourses} />
        <ExpertRoute exact path="/create-course" component={CreateCourse} />
        <ExpertRoute exact path="/edit-course/:id" component={CreateCourse} />
        <Route exact path="/policy" component={Policy} />
        <Route component={NotFound} />
      {/*TODO: footer*/}
      </Switch>
    </section>
  )
}

export default Routes
