import React, { useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCurrentExpert, getExperts } from '../../actions/expert';

const ExpertRoute = ({
  getExperts,
  getCurrentExpert,
  component: Component,
  auth: { isAuthenticated, user },
  expert: { expert, experts },
  ...rest
}) => {
  useEffect(() => {
    getExperts();
  }, [getExperts])
  useEffect(() => {
    if (isAuthenticated && user && experts && experts.find(e => e.expert_user && e.expert_user._id === user._id)) {
      getCurrentExpert();
    }
  }, [getCurrentExpert, isAuthenticated, user, experts]);
  return (
  <Route
    {...rest}
    render={props =>
      isAuthenticated && expert && expert.expert_user ?
        <Component {...props} /> :
        <Redirect to='/login' />
    }
  />
)};

ExpertRoute.propTypes = {
  getExperts: PropTypes.func.isRequired,
  getCurrentExpert: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  expert: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  expert: state.expert
})

export default connect(mapStateToProps, { getCurrentExpert, getExperts })(ExpertRoute);
