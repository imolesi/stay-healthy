import React from 'react';
import { Link } from 'react-router-dom';

const DashboardActions = () => {
  return (
    <div className="dash-buttons">
      <Link to="/edit-profile" className="btn btn-light">
        <i className="fas fa-user-circle text-primary"></i> Aggiorna Profilo
      </Link>
      <Link to="/edit-client-interests" className="btn btn-light">
        <i className="fas fa-exclamation text-primary"></i> Aggiorna Interessi
      </Link>
      <Link to="/edit-client-objectives" className="btn btn-light">
        <i className="fas fa-bullseye text-primary"></i> Aggiorna Obiettivi
      </Link>
    </div>
  )
}

export default DashboardActions
