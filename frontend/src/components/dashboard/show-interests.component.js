import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getInterests } from '../../actions/tag';
import { deleteInterest } from '../../actions/client';

const ShowInterests = ({ getInterests, interests, tags: { tags }, deleteInterest }) => {
  useEffect(() => {
    getInterests();
  }, [getInterests])
  const interestList = interests.map(int => {
    const i = tags.length > 0 ? tags.find(e => e._id === int.tag) : "";
    return (<div className="row" key={int._id}>
      <div className="col-12 col-md-9">{i && i.tag_name}</div>
      <div className="col-12 col-md-3">
        <button onClick={() => deleteInterest(int._id)} className="btn btn-danger">Elimina</button>
      </div>
    </div>);
  });
  return (
    <Fragment>
      <p className="area-title">Interessi</p>
      <div className="center">
        <div className="container table">
            {interestList}
        </div>
      </div>
    </Fragment>
  )
};

ShowInterests.propTypes = {
  getInterests: PropTypes.func.isRequired,
  interests: PropTypes.array.isRequired,
  deleteInterest: PropTypes.func.isRequired,
  tags: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  tags: state.tags
});

export default connect(
  mapStateToProps,
  { getInterests, deleteInterest }
)(ShowInterests);
