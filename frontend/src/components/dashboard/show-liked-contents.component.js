import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import { getArticles } from '../../actions/article';
import Moment from 'react-moment';

const ShowLikedContents = ({ getArticles, article: { articles, loading }, auth }) => {
  useEffect(() => {
    getArticles();
  }, [getArticles]);

  const findUserLike = likes => {
    if (likes.filter(like => auth && auth.user && like.user === auth.user._id).length > 0) {
      return true;
    } else {
      return false;
    }
  }

  return (
    loading ? <Spinner /> : (
      <Fragment>
        <div className="liked-articles">
          <div className="center">
          <div className="container table">
          {
              articles && articles.length > 0 ?
              articles.sort((a1, a2) => a1.article_publicationDate - a2.article_publicationDate)
              .filter(a => auth && auth.user && findUserLike(a.article_likes))
              .map((article, key) =>
                  <div className="row" key={key}>
                    <div className="col-12 col-md-9"><Link to={`/edit-article/${article._id}`} className="">{article.article_title}</Link></div>
                    <div className="col-12 col-md-3"><Moment format="DD/MM/YYYY">{article.article_publicationDate}</Moment></div>
                  </div>
              ) : <div className="row">Nessun articolo pubblicato</div>
          }
          </div>
          </div>
        </div>
      </Fragment>
    )
  );
};

ShowLikedContents.propTypes = {
  getArticles: PropTypes.func.isRequired,
  article: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  article: state.article,
  auth: state.auth
});

export default connect(mapStateToProps, { getArticles })(ShowLikedContents);
