import React, { Fragment, useEffect, useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import ShowInterests from './show-interests.component.js';
import ShowObjectives from './show-objectives.component.js';
import ShowLikedContents from './show-liked-contents.component.js';
import ShowAchievements from './show-achievements.component.js';
import EditClient from "./../client-forms/edit-client.component";
import EditClientInterests from "./../client-forms/edit-client-interests.component";
import EditClientObjectives from "./../client-forms/edit-client-objectives.component";
import EditExpert from "./../expert-forms/edit-expert.component";
import { addWeightProgress, getCurrentClient, getClients, deleteWeightProgress, deleteAccount } from '../../actions/client';
import { getExperts, getCurrentExpert, deleteExpert } from '../../actions/expert'
import { getCourses } from '../../actions/course';
import { getDiets } from '../../actions/diet';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import * as moment from 'moment';

import CanvasJSReact from '../../lib/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const Dashboard = ({
  addWeightProgress,
  getDiets,
  getCourses,
  getCurrentClient,
  getClients,
  getCurrentExpert,
  getExperts,
  deleteWeightProgress,
  deleteAccount,
  auth: { user, isAuthenticated },
  client: { client, clients, loading },
  expert: { expert, experts },
  diet,
  course,
  history
}) => {

  const firstTabRef = useRef();
  const firstTabRefCurrent = firstTabRef && firstTabRef.current;

  useEffect(() => {
    firstTabRef && firstTabRefCurrent && firstTabRefCurrent.click()
  }, [firstTabRef, firstTabRefCurrent])

  useEffect(() => {
    getClients();
    getExperts();
    getCourses();
    setFormData({
      client_currentWeight: ''
    });
  }, [getClients, getExperts, getCourses]);

  useEffect(() => {
    if (isAuthenticated && user && clients && clients.find(e => e.client_user && e.client_user._id === user._id)) {
      getCurrentClient();
    }
  }, [getCurrentClient, isAuthenticated, user, clients]);

  useEffect(() => {
    if (isAuthenticated && user && experts && experts.find(e => e.expert_user && e.expert_user._id === user._id)) {
      getCurrentExpert();
    }
  }, [getCurrentExpert, isAuthenticated, user, experts]);

  const [formData, setFormData] = useState({
    client_currentWeight: ''
  });

  const {
    client_currentWeight
  } = formData;

  const onChange = e => setFormData({
    ...formData, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    addWeightProgress(formData, history);
  }

  const onClick = async e => {
    e.preventDefault();
    client && client.client_weightProgress.length > 0 && deleteWeightProgress(client.client_weightProgress.slice(-1).pop()._id);
  }

  function parseISOString(s) {
    var b = s.split(/\D+/);
    return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
  }

  function sameDay(d1, d2) {
    return d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate();
  }

  const options = {
			animationEnabled: true,
      theme: "light2",
			title:{
				text: "Progressi peso corporeo"
			},
			axisY: {
				title: "Peso",
				suffix: "Kg"
			},
			axisX: {
				title: "Data"
      },
			data: [{
				type: "line",
				toolTipContent: "Data {x}: {y}%",
				dataPoints: []
			}]
	}

  const events = [];
  course.courses.filter(e => e.course_subscribers.filter(e => user && user._id && e.subscriber === user._id).length > 0)
    .forEach(e => e.course_days.forEach(f => {
        var start = moment(e.course_startDate);
        var end = moment(e.course_endDate);
        var day = f;
        var current = start.clone();
        while (current.day(7 + day).isBefore(end)) {
          events.push({ title: e.course_name, date: current.format("YYYY-MM-DD") });
        }
    }));

  client && client.client_weightProgress && client.client_weightProgress.forEach(e => options.data[0].dataPoints
    .push({
      x: new Date(parseISOString(e.date).getUTCFullYear(),
        parseISOString(e.date).getUTCMonth(),
        parseISOString(e.date).getUTCDate()),
      y: e.weight
    }));

  const openTab = async (e, tabName) => {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    e.currentTarget.className += " active";
  }

  return loading && client === null ?
    <Spinner /> :
    <Fragment>
      <h1 className="large text-primary my-3">Area personale</h1>
      <p className="lead">
        <i className="fas fa-user" /> { user && (user.user_gender === "female" ? "Bentornata" : "Bentornato")} { user && user.user_name }!
      </p>
      {client !== null ?
        <Fragment>
          {expert ? "" : <Fragment>
            <p>Sei un esperto? Crea un profilo apposito!</p>
            <Link to='create-expert' className="btn btn-primary my-1">
              Richiedi abilitazione
            </Link>
          </Fragment>}
          <div className="tab my-3">
            <button ref={firstTabRef} className="tablinks" onClick={e => openTab(e, 'weight')}>Progressi peso</button>
            <button className="tablinks" onClick={e => openTab(e, 'calendar')}>Calendario</button>
            <button className="tablinks" onClick={e => openTab(e, 'interests')}>Interessi</button>
            <button className="tablinks" onClick={e => openTab(e, 'objectives')}>Obiettivi</button>
            <button className="tablinks" onClick={e => openTab(e, 'contents')}>Contenuti apprezzati</button>
            <button className="tablinks" onClick={e => openTab(e, 'achievement')}>Achievement</button>
            <button className="tablinks" onClick={e => openTab(e, 'profile')}>Profilo</button>
            {expert && <button className="tablinks" onClick={e => openTab(e, 'expert')}>Dati esperto</button>}
          </div>
          <div id="weight" className="tabcontent">
            <p className="text-primary area-title">
              Tieni traccia del tuo peso corporeo
            </p>
          { client.client_weightProgress.length === 0 || (client.client_weightProgress.length > 0 && !sameDay(new Date(), parseISOString(client.client_weightProgress.slice(-1).pop().date))) ? (
              <div>
                <p className="area-title">Oggi ti sei pesato?</p>
                  <form className="form" onSubmit={e => onSubmit(e)}>
                  <div className="form-group">
                    <label htmlFor="client_currentWeight" className="d-none">Inserisci il tuo peso attuale</label>
                    <input
                      type="number"
                      placeholder="Inserisci il tuo peso attuale"
                      id="client_currentWeight"
                      name="client_currentWeight"
                      value={client_currentWeight}
                      onChange={e => onChange(e)}
                    />
                  </div>
                  <input type='submit' value="Inserisci" className='btn btn-primary my-1' />
                </form>
              </div>
            ) : (
              <div className="form-group">
                <p className="area-title">Peso odierno aggiornato: {client.client_weightProgress.length > 0 && client.client_weightProgress.slice(-1).pop().weight}Kg</p>
                <input type='button' value="Reinserisci peso" className='btn btn-primary my-1' onClick={e => onClick(e)} />
              </div>
            )
          }
            <div className="weight-chart">
              {<CanvasJSChart options={options} />}
		        </div>
          </div>
          <div id="calendar" className="tabcontent">
            <p className="area-title text-primary">
              Calendario dei corsi e delle attività
            </p>
            <FullCalendar
              plugins={[ dayGridPlugin ]}
              initialView="dayGridMonth"
              events={events}
              height="100%"
              contentHeight="auto"
            />
          </div>
          <div id="interests" className="tabcontent">
            <p className="area-title text-primary">
              Mantieni aggiornati i tuoi interessi
            </p>
            <EditClientInterests />
            <ShowInterests interests={client.client_interests} />
          </div>
          <div id="objectives" className="tabcontent">
            <p className="area-title text-primary">
              Mantieni aggiornati i tuoi obiettivi
            </p>
            <EditClientObjectives />
            <ShowObjectives objs={client.client_objectives} />
          </div>
          <div id="contents" className="tabcontent">
            <p className="area-title text-primary">
              Contenuti apprezzati
            </p>
            <ShowLikedContents />
          </div>
          <div id="achievement" className="tabcontent">
            <p className="area-title text-primary">
              Svolgi diverse azioni per ottenere nuovi achievement!
            </p>
            <ShowAchievements achs={client.client_achievements} />
          </div>
          <div id="profile" className="tabcontent">
            <p className="area-title text-primary">
              Mantieni aggiornato il tuo profilo
            </p>
            <EditClient />
            <div className="my-2">
              <button className="btn btn-danger" onClick={() => deleteAccount()}>
                <i className="fas fa-user-minus"></i> Elimina il mio account
              </button>
            </div>
          </div>
          {expert && <div id="expert" className="tabcontent">
            <p className="area-title text-primary">
              Mantieni aggiornati i tuoi dati di esperto
            </p>
            <EditExpert />
            <div className="my-2">
              <button className="btn btn-danger" onClick={() => deleteExpert()}>
                <i className="fas fa-user-minus"></i> Elimina il mio account di esperto
              </button>
            </div>
          </div>}
        </Fragment> :
        <Fragment>
          <p>Non hai ancora configurato il tuo profilo, conosciamoci meglio!</p>
          <Link to='create-profile' className="btn btn-primary my-1">
            Crea profilo
          </Link>
        </Fragment>
      }
    </Fragment>;
};

Dashboard.propTypes = {
  addWeightProgress: PropTypes.func.isRequired,
  getCurrentClient: PropTypes.func.isRequired,
  getClients: PropTypes.func.isRequired,
  getCurrentExpert: PropTypes.func.isRequired,
  getExperts: PropTypes.func.isRequired,
  getDiets: PropTypes.func.isRequired,
  getCourses: PropTypes.func.isRequired,
  deleteWeightProgress: PropTypes.func.isRequired,
  deleteAccount: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  client: PropTypes.object.isRequired,
  diet: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  client: state.client,
  expert: state.expert,
  diet: state.diet,
  course: state.course
})

export default connect(mapStateToProps, { addWeightProgress, getCourses, getDiets, getCurrentClient, getClients, getCurrentExpert, getExperts, deleteWeightProgress, deleteAccount })(Dashboard)
