import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getAchievements } from '../../actions/achievement';

const ShowAchievements = ({ getAchievements, achs, achievement: { achievements } }) => {
  useEffect(() => {
    getAchievements();
  }, [getAchievements])
  const achievementList = achs.map(ach => {
    const a = achievements.length > 0 ? achievements.find(a => a._id === ach.achievement) : "";
    return (<div className="row" key={ach._id}>
      <div className="col-6 col-md-3"><img src={a && a.achievement_image} alt={a && a.achievement_name}/></div>
      <div className="col-12 col-md-3">{a && a.achievement_name}</div>
      <div className="col-12 col-md-6">{a && a.achievement_description}</div>
    </div>);
  });
  return (
    <Fragment>
      <div className="container table">
          {achievementList}
      </div>
    </Fragment>
  )
};

ShowAchievements.propTypes = {
  getAchievements: PropTypes.func.isRequired,
  achs: PropTypes.array.isRequired,
  achievement: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  achievement: state.achievement
});

export default connect(
  mapStateToProps,
  { getAchievements }
)(ShowAchievements);
