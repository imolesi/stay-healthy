import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getObjectives } from '../../actions/objective';
import { deleteObjective } from '../../actions/client';

const ShowObjectives = ({ getObjectives, objs, objectives: { objectives }, deleteObjective }) => {
  useEffect(() => {
    getObjectives();
  }, [getObjectives])
  const objectiveList = objs.map(obj => {
    const o = objectives.length > 0 ? objectives.find(e => e._id === obj.objective) : "";
    return (<div className="row" key={obj._id}>
      <div className="col-12 col-md-9">{o && o.objective_name}</div>
      <div className="col-12 col-md-3">
        <button onClick={() => deleteObjective(obj._id)} className="btn btn-danger">Elimina</button>
      </div>
    </div>);
  });
  return (
    <Fragment>
      <p className="area-title">Obiettivi</p>
      <div className="center">
        <div className="container table">
            {objectiveList}
        </div>
      </div>
    </Fragment>
  )
};

ShowObjectives.propTypes = {
  getObjectives: PropTypes.func.isRequired,
  objs: PropTypes.array.isRequired,
  deleteObjective: PropTypes.func.isRequired,
  objectives: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  objectives: state.objectives
});

export default connect(
  mapStateToProps,
  { getObjectives, deleteObjective }
)(ShowObjectives);
