import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getDiets } from '../../actions/diet';

const ShowDiet = ({ getDiets, diet: { diets }, d }) => {
  useEffect(() => {
    getDiets();
  }, [getDiets])
  const diet = d && diets.length > 0 && diets.find(e => e._id === d && e.diet_name)
  return (
    <Fragment>
      <p className="area-title">Dieta</p>
      <div>
        { diet && diet.diet_name }
      </div>
    </Fragment>
  )
};

ShowDiet.propTypes = {
  getDiets: PropTypes.func.isRequired,
  d: PropTypes.string.isRequired,
  diet: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  diet: state.diet
});

export default connect(mapStateToProps, { getDiets })(ShowDiet)
