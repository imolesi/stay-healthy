import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import { getArticles } from '../../actions/article';
import Moment from 'react-moment';

const ManageArticles = ({ getArticles, article: { articles, loading }, auth }) => {
  useEffect(() => {
    getArticles();
  }, [getArticles]);
  return (
    loading ? <Spinner /> : (
      <Fragment>
        <h1 className="large text-primary my-3">Gestisci articoli</h1>
        <div>
          <Link to={`/create-article`} className="btn btn-dark btn-lg my-2">
            Crea nuovo articolo
          </Link>
          <p className="my-3 area-title">Gestisci articoli pubblicati</p>
          <div className="center">
            <div className="container table">
            {
                articles && articles.length > 0 ?
                articles.sort((a1, a2) => a1.article_publicationDate - a2.article_publicationDate)
                .filter(a => auth && auth.user && a.article_author === auth.user._id)
                .map((article, key) =>
                    <div className="row" key={key}>
                      <div className="col-12 col-md-9"><Link to={`/edit-article/${article._id}`} className="">{article.article_title}</Link></div>
                      <div className="col-12 col-md-3"><Moment format="DD/MM/YYYY">{article.article_publicationDate}</Moment></div>
                    </div>
                ) : <div className="row">Nessun articolo</div>
            }
            </div>
          </div>
        </div>
      </Fragment>
    )
  );
};

ManageArticles.propTypes = {
  getArticles: PropTypes.func.isRequired,
  article: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  article: state.article,
  auth: state.auth
});

export default connect(mapStateToProps, { getArticles })(ManageArticles);
