import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import ArticleItem from './article-item.component';
import { getArticles } from '../../actions/article';
import { getExperts } from '../../actions/expert';

const Articles = ({ getArticles, getExperts, article: { articles, loading }, expert: { experts } }) => {

  useEffect(() => {
    getExperts();
    getArticles();
  }, [getExperts, getArticles]);

  const [formData, setFormData] = useState({
    article_search: ''
  });

  const {
    article_search
  } = formData;

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  return (
    loading ? <Spinner /> : (
      <Fragment>
        <h1 className="title">Cerchi un articolo in particolare?</h1>
        <form className="form">
          <div className="form-group">
            <label htmlFor="article_search" className="d-none">
              Cerca per titolo
            </label>
            <input
              type="text"
              placeholder="Cerca per titolo"
              id="article_search"
              name="article_search"
              value={article_search}
              onChange={e => onChange(e)}
              className="search-text"
            />
            <span className="fas fa-search search-icon" />
          </div>
        </form>
        <div className="item">
          {
            articles && articles.filter(e => e.article_title.toLowerCase().includes(article_search)).map(function(article) {
              const exp = experts.find(e => e.expert_user._id === article.article_author);
              return exp && (<ArticleItem key={article._id} article={article} expert={exp} />)
            })
          }
        </div>
      </Fragment>
    )
  );
};

Articles.propTypes = {
  getArticles: PropTypes.func.isRequired,
  getExperts: PropTypes.func.isRequired,
  article: PropTypes.object.isRequired,
  expert: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  article: state.article,
  expert: state.expert
});

export default connect(mapStateToProps, { getExperts, getArticles })(Articles);
