import React, { Fragment, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/spinner.component';
import { addArticle, updateArticle, deleteArticle, getArticle, clearArticle } from '../../actions/article';
import { getInterests } from '../../actions/tag';
import { setAlert } from '../../actions/alert';
import { Editor } from '@tinymce/tinymce-react';

const CreateArticle = ({ addArticle, updateArticle, deleteArticle, getArticle, clearArticle, getInterests, setAlert, auth: { isAuthenticated }, expert: { expert }, article: { article, loading }, tags: { tags }, match, history }) => {

  const initialState = {
    article_title: '',
    article_description: '',
    article_tags: []
  }

  const [formData, setFormData] = useState(initialState);

  const {
    article_title,
    article_description,
    article_tags
  } = formData;

  const handleSelect = (e, tag) => {
    const { checked } = e.target
    setFormData(state => ({
      ...state,
      article_tags: {
        ...state.article_tags,
        [tag._id]: checked
      }
    }))
  }

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    if (!article_description) {
      setAlert('Scrivi una descrizione', 'danger');
    } else {
      const newTags = Object.keys(formData.article_tags).filter(t => formData.article_tags[t]).map(t => JSON.parse(JSON.stringify({ "tag" : t })))
      if (newTags.length > 0) {
        if (match && match.params && match.params.id) {
          updateArticle(match.params.id, { ...formData, article_tags: newTags }, history);
        } else {
          addArticle({ ...formData, article_tags: newTags }, history);
        }
      } else {
        setAlert('Seleziona almeno un interesse', 'danger');
      }
    }
  };

  const handleEditorChange = e => {
    setFormData({ ...formData, article_description: e });
  }

  useEffect(() => {
    setFormData({
      article_title: article ? article.article_title : '',
      article_description: article ? article.article_description : '',
      article_tags: tags.reduce((state, tag) =>
            ({ ...state, [tag._id]: article && article.article_tags ? article.article_tags.map(t => t.tag).includes(tag._id) : false }),
        {}
      )
    });
  }, [article, tags]);
  useEffect(() => {
    getInterests();
    if (match && match.params && match.params.id) {
      getArticle(match.params.id);
    } else {
      clearArticle();
    }
  }, [getInterests, getArticle, clearArticle, match])

  return (
    (!isAuthenticated || loading) ? <Spinner /> : (<Fragment>
      <div className="post-form">
        <h1 className="large text-primary">{match && match.params && match.params.id && article && article_title ? "Modifica \"" + article_title + "\"": "Scrivi il tuo articolo"}</h1>
        <form className="form my-1" onSubmit={e => onSubmit(e)}>
          <div className="form-group">
            <label htmlFor="article_title" className="d-none">Scrivi il titolo</label>
            <textarea
            id="article_title"
            name="article_title"
            cols="30"
            rows="1"
            placeholder="Titolo"
            value={article_title}
            onChange={e => onChange(e)}
            required>
            </textarea>
          </div>
          <div className="form-group">
            <Editor
              initialValue={article && article.article_description && "<p>"+article.article_description+"</p>"}
              apiKey="n6y968kozaobofk1ue36r8kjd5ubagvgcnjf0j8fr045pur5"
              init={{
                height: 500,
                menubar: false,
                plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table paste code help wordcount'
                ],
                toolbar:
                  'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                placeholder: 'Scrivi qui il tuo articolo'
                }}
              onEditorChange={e => handleEditorChange(e)}
            />
          </div>
          <div className="form-group">
            <fieldset>
              <legend>Seleziona i tag relativi</legend>
              {
                tags
                .sort()
                .map((tag, key) => {
                  const tagCheck = article_tags[tag._id]
                  return (
                    <label className="m-1 cb-item"key={key}>
                      {tag.tag_name}
                    <input
                      name={tag.tag_name}
                      type="checkbox"
                      checked={tagCheck ? tagCheck : false}
                      onChange={e => handleSelect(e, tag)}
                      disabled={expert && expert.expert_knowledge && !expert.expert_knowledge.filter(k => k.tag).map(k => k.tag).includes(tag._id)}
                      className="mx-3"
                    /></label>
                  );
                })
              }
            </fieldset>
          </div>
          {match && match.params && match.params.id && (<div className="my-2">
              <button type="button" className="btn btn-danger" onClick={() => deleteArticle(match.params.id, history)}>
                <i className="fas"></i> Elimina questo articolo
              </button>
            </div>)}
          <input type="submit" className="btn btn-dark my-1" value={(match && match.params && match.params.id) ? "Aggiorna" : "Crea"} />
        </form>
      </div>
    </Fragment>)
  )
}

CreateArticle.propTypes = {
  auth: PropTypes.object.isRequired,
  expert: PropTypes.object.isRequired,
  article: PropTypes.object.isRequired,
  tags: PropTypes.object.isRequired,
  addArticle: PropTypes.func.isRequired,
  updateArticle: PropTypes.func.isRequired,
  deleteArticle: PropTypes.func.isRequired,
  getArticle: PropTypes.func.isRequired,
  clearArticle: PropTypes.func.isRequired,
  getInterests: PropTypes.func.isRequired,
  setAlert: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  expert: state.expert,
  article: state.article,
  tags: state.tags
})

export default connect(mapStateToProps, { addArticle, updateArticle, deleteArticle, getArticle, clearArticle, getInterests, setAlert })(withRouter(CreateArticle))
