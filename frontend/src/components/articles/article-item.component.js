import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import Moment from 'react-moment';
import { connect } from 'react-redux';
import { addLike, removeLike, deleteArticle } from '../../actions/article';
import { getInterests } from '../../actions/tag';
import { getClients } from '../../actions/client';

const ArticleItem = ({
  addLike,
  removeLike,
  deleteArticle,
  getInterests,
  getClients,
  auth,
  expert,
  client: { clients },
  article: {
    _id,
    article_title,
    article_description,
    article_author,
    article_avatar,
    article_publicationDate,
    article_likes,
    article_tags,
    article_comments
  },
  tags: { tags },
  history,
  showViewComments
}) => {

  useEffect(() => {
    getClients();
    getInterests();
  }, [getClients, getInterests])

  const findUserLike = likes => {
    if (likes.filter(like => auth && auth.user && like.user === auth.user._id).length > 0) {
      return true;
    } else {
      return false;
    }
  }

  const client = clients && clients.find(e => e.client_user._id === article_author);

  return (
    <div className="post">
      <div className="item">
        <Link to={`/articles/${_id}`}>
          <span className="item-title">{article_title}</span>
        </Link>
        <p className="post-date">
          Pubblicato il <Moment format="DD/MM/YYYY">{article_publicationDate}</Moment>
        </p>
        {<span dangerouslySetInnerHTML={{__html: article_description}} />}
        <div>
          {
            tags && article_tags && tags.filter(t => article_tags.map(t2 => t2.tag).includes(t._id))
              .map((t, key) => <span key={key} className="badge badge-info mx-1">{t.tag_name}</span>)
          }
        </div>
      </div>
      <div className="author bg-light">
        { client && (
          <Link to={`/profile/${article_author}`}>
              <img
                className="author-image"
                src={article_avatar}
                alt={article_author}
              />
              <div className="author-name">di {expert && (expert.expert_user.user_name + ' ' + expert.expert_user.user_lastname)}</div>
              <div className="author-bio">{expert.expert_bio}</div>
          </Link>
        )}
      </div>
      <div className="article-buttons">
        <button onClick={e => addLike(_id)} type="button" className="btn btn-light" disabled={!auth.isAuthenticated || findUserLike(article_likes)}>
          <i className="fas fa-thumbs-up"></i>
          {' '}
          {article_likes && article_likes.length > 0 && article_likes.length}
        </button>
        <button onClick={e => removeLike(_id)} type="button" className="btn btn-light" disabled={!auth.isAuthenticated || !findUserLike(article_likes)}>
          <i className="fas fa-thumbs-down"></i>
        </button>
        {showViewComments &&
          <Fragment>
            <Link to={`/articles/${_id}`} className="btn btn-dark">
              Vedi commenti{' '}
              {article_comments && article_comments.length > 0 && (
                <span className="comment-count">{article_comments.length}</span>
              )}
            </Link>
          </Fragment>
        }
        { auth && auth.user && auth.isAuthenticated && !auth.loading && article_author === auth.user._id && (
          <button onClick={e => deleteArticle(_id, history)} type="button" className="btn btn-danger">
            <i className="fas fa-times"></i>
          </button>
        )}
      </div>
    </div>
  )
}

ArticleItem.defaultProps = {
  showViewComments: true
}

ArticleItem.propTypes = {
  expert: PropTypes.object.isRequired,
  article: PropTypes.object.isRequired,
  tags: PropTypes.object.isRequired,
  client: PropTypes.object.isRequired,
  addLike: PropTypes.func.isRequired,
  removeLike: PropTypes.func.isRequired,
  deleteArticle: PropTypes.func.isRequired,
  getInterests: PropTypes.func.isRequired,
  getClients: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  tags: state.tags,
  client: state.client
})

export default connect(mapStateToProps, { addLike, removeLike, deleteArticle, getInterests, getClients })(withRouter(ArticleItem))
