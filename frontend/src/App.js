import React, { Fragment, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { Provider } from 'react-redux';
import store from './store';
import { loadUser, logout } from './actions/auth';
import setAuthToken from './utils/setAuthToken';
import jwt_decode from 'jwt-decode';

import "bootstrap/dist/css/bootstrap.min.css";

import Navbar from "./components/layout/navbar.component";
import Routes from "./components/routing/routes.component";
import Footer from "./components/layout/footer.component";

// Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>

const App = () => {
  useEffect(() => {
    setAuthToken(localStorage.token);
    if (localStorage.token) {
      const decodedToken = jwt_decode(localStorage.token);
      const currentTime = Date.now() / 1000;
      if (decodedToken.exp < currentTime) {
        setAuthToken(false);
        store.dispatch(logout());
        window.location.href = '/login';
      } else {
        store.dispatch(loadUser());
      }
    }
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Navbar />
          <Switch>
            <Route component={Routes} />
          </Switch>
          <Footer />
        </Fragment>
      </Router>
    </Provider>
  );
};

export default App;
