import api from './utils/api';
import { host } from './utils/host'
const address = 'http://' + host + ':4000';

const publicVapidKey =
  'BP-iUCRU8RoQ1kRMAXOCIbHgpV1bF6F7uU-R3ZLob4BU1fYF9u24InRgi0VKOAZ4NQtS0kb-EHGUDVUkYA79jS0';

const serviceWorkerRegister = (userId) => {
  if ('serviceWorker' in navigator) {
    send(userId).catch(err => console.error(err));
  }
};

// registers sw, push and send notification
async function send(userId) {
  //console.log('Registering service worker...');
  await navigator.serviceWorker.register('/worker.js', {
    scope: '/'
  });
  //console.log('Service worker registered');
  const register = await navigator.serviceWorker.ready;
  //console.log('Service worker ready');
  //console.log('Registering push...');
  const subscription = await register.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
  });
  //console.log('Push registered');
  const headers = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  await api.post(address + '/users/subscriptions/'+userId, JSON.stringify(subscription), headers);
  //console.log('Push saved in DB');
}

// Web-Push
// Public base64 to Uint
function urlBase64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

export default serviceWorkerRegister;
