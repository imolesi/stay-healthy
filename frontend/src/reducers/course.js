import {
    GET_COURSES,
    COURSE_ERROR,
    DELETE_COURSE,
    ADD_COURSE,
    UPDATE_COURSE,
    GET_COURSE,
    SUBSCRIBE,
    UNSUBSCRIBE,
    ADD_COURSE_OBJECTIVE,
    REMOVE_COURSE_OBJECTIVE,
    CLEAR_COURSE
} from '../actions/types';

const initialState = {
    courses: [],
    course: null,
    loading: true,
    error: {}
};

const courseReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
      case GET_COURSES:
          return {
              ...state,
              courses: payload,
              loading: false
          };
      case GET_COURSE:
          return {
              ...state,
              course: payload,
              loading: false
          };
      case ADD_COURSE:
          return {
              ...state,
              courses: [payload, ...state.courses],
              loading: false
          };
      case UPDATE_COURSE:
          return {
              ...state,
              courses: state.courses.map(c => c._id === payload.id ?
              { ...payload.course } : c),
              loading: false
          };
      case DELETE_COURSE:
          return {
              ...state,
              courses: state.courses.filter(course => course._id !== payload),
              loading: false
          };
      case COURSE_ERROR:
          return {
              ...state,
              error: payload,
              loading: false
          };
      case SUBSCRIBE:
          return {
              ...state,
              courses: state.courses.map(c => c._id === payload.courseId ?
                  { ...c, course_subscribers: payload.subscribers } : c),
              loading: false
          };
      case UNSUBSCRIBE:
          return {
              ...state,
              courses: state.courses.map(c => c._id === payload.courseId ?
                  { ...c, course_subscribers: c.course_subscribers.filter(sub => sub.subscriber !== payload.subscribers.unsubscribed) } : c),
              loading: false
          };
      case ADD_COURSE_OBJECTIVE:
          return {
              ...state,
              course: { ...state.course, course_objectives: payload },
              loading: false
          };
      case REMOVE_COURSE_OBJECTIVE:
          return {
              ...state,
              course: {
                  ...state.course,
                  course_objectives: state.course.course_objectives.filter(obj => obj.objective !== payload)
              },
              loading: false
          };
      case CLEAR_COURSE:
          return {
              ...state,
              course: null,
              loading: false
          };
      default:
          return state;
  }
}

export default courseReducer;
