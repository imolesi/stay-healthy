import { combineReducers } from 'redux';
import alert from './alert';
import auth from './auth';
import client from './client';
import expert from './expert';
import article from './article';
import course from './course';
import tags from './tags';
import objectives from './objectives';
import diet from './diet';
import achievement from './achievement';

export default combineReducers({
  alert,
  auth,
  client,
  expert,
  article,
  course,
  tags,
  objectives,
  diet,
  achievement
});
