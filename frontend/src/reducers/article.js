import {
  GET_ARTICLES,
  ARTICLE_ERROR,
  UPDATE_LIKES,
  DELETE_ARTICLE,
  ADD_ARTICLE,
  UPDATE_ARTICLE,
  GET_ARTICLE,
  ADD_COMMENT,
  REMOVE_COMMENT,
  ADD_ARTICLE_TAG,
  REMOVE_ARTICLE_TAG,
  CLEAR_ARTICLE
} from '../actions/types';

const initialState = {
  articles: [],
  article: null,
  loading: true,
  error: {}
};

const articleReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case GET_ARTICLES:
      return {
        ...state,
        articles: payload,
        loading: false
      };
    case GET_ARTICLE:
      return {
        ...state,
        article: payload,
        loading: false
      };
    case ADD_ARTICLE:
      return {
        ...state,
        articles: [payload, ...state.articles],
        loading: false
      };
    case UPDATE_ARTICLE:
      return {
        ...state,
        articles: state.articles.map(art => art._id === payload.id ?
          { ...payload.article } : art),
        loading: false
      };
    case DELETE_ARTICLE:
      return {
        ...state,
        articles: state.articles.filter(article => article._id !== payload),
        loading: false
      };
    case ARTICLE_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      };
    case UPDATE_LIKES:
      return {
        ...state,
        articles: state.articles.map(art => art._id === payload.id ?
          { ...art, article_likes: payload.likes } : art),
        loading: false
      };
    case ADD_COMMENT:
      return {
        ...state,
        article: { ...state.article, article_comments: payload.comments ? payload.comments : [] },
        loading: false
      };
    case REMOVE_COMMENT:
      return {
        ...state,
        article: {
          ...state.article,
          article_comments: state.article.article_comments.filter(comment => comment._id !== payload)
        },
        loading: false
      };
    case ADD_ARTICLE_TAG:
      return {
        ...state,
        article: { ...state.article, article_tags: payload },
        loading: false
      };
    case REMOVE_ARTICLE_TAG:
      return {
        ...state,
        article: {
          ...state.article,
          article_comments: state.article.article_tags.filter(t => t.tag !== payload)
        },
        loading: false
      };
    case CLEAR_ARTICLE:
      return {
        ...state,
        article: null,
        loading: false
      };
    default:
      return state;
  }
}

export default articleReducer;
