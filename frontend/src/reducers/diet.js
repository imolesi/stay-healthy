import {
  GET_DIETS,
  DIETS_ERROR
} from '../actions/types';

const initialState = {
  diet: null,
  diets: [],
  loading: true,
  error: {}
};

const dietReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case GET_DIETS:
      return {
        ...state,
        diets: payload,
        loading: false
      };
    case DIETS_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      };
    default:
      return state;
  }
}

export default dietReducer;
