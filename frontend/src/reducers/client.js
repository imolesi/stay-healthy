import {
  GET_CLIENT,
  GET_CLIENTS,
  CLIENT_ERROR,
  CLEAR_CLIENT,
  UPDATE_CLIENT
} from '../actions/types';

const initialState = {
  client: null,
  clients: [],
  loading: true,
  error: {}
};

const clientReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case GET_CLIENT:
    case UPDATE_CLIENT:
      return {
        ...state,
        client: payload,
        loading: false
      };
    case GET_CLIENTS:
      return {
        ...state,
        clients: payload,
        loading: false
      };
    case CLIENT_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
        client: null
      };
    case CLEAR_CLIENT:
      return {
        ...state,
        client: null,
        loading: false
      };
    default:
      return state;
  }
}

export default clientReducer;
