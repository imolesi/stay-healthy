import {
    GET_EXPERT,
    GET_EXPERTS,
    EXPERT_ERROR,
    CLEAR_EXPERT,
    UPDATE_EXPERT,
    EXPERT_DELETE
  } from '../actions/types';

  const initialState = {
    expert: null,
    experts: [],
    loading: true,
    error: {}
  };

const expertReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case GET_EXPERT:
    case UPDATE_EXPERT:
      return {
        ...state,
        expert: payload,
        loading: false
      };
    case GET_EXPERTS:
      return {
        ...state,
        experts: payload,
        loading: false
      };
    case EXPERT_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
        expert: null
      };
    case CLEAR_EXPERT:
      return {
        ...state,
        expert: null,
        loading: false
      };
     case EXPERT_DELETE:
       return {
         ...state,
         experts: state.experts.filter(expert => expert._id !== payload._id),
         loading: false
       };
    default:
      return state;
  }
}

export default expertReducer;
