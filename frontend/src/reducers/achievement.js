import {
  GET_ACHIEVEMENTS,
  ACHIEVEMENTS_ERROR
} from '../actions/types';

const initialState = {
  achievement: null,
  achievements: [],
  loading: true,
  error: {}
};

const achievementReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case GET_ACHIEVEMENTS:
      return {
        ...state,
        achievements: payload,
        loading: false
      };
    case ACHIEVEMENTS_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      };
    default:
      return state;
  }
}

export default achievementReducer;
