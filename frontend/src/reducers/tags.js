import {
  GET_TAGS,
  TAGS_ERROR
} from '../actions/types';

const initialState = {
  tags: [],
  loading: true,
  error: {}
};

const tagsReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case GET_TAGS:
      return {
        ...state,
        tags: payload,
        loading: false
      };
    case TAGS_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      };
    default:
      return state;
  }
}

export default tagsReducer;
