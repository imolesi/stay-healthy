import {
  GET_OBJECTIVES,
  OBJECTIVES_ERROR
} from '../actions/types';

const initialState = {
  objectives: [],
  loading: true,
  error: {}
};

const objectivesReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case GET_OBJECTIVES:
      return {
        ...state,
        objectives: payload,
        loading: false
      };
    case OBJECTIVES_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      };
    default:
      return state;
  }
}

export default objectivesReducer;
