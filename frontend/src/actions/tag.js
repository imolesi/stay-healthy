import api from '../utils/api';
import {
  GET_TAGS,
  TAGS_ERROR
} from './types';
import { host } from '../utils/host'
const address = 'http://' + host + ':4000';

export const getInterests = () => async dispatch => {
  try {
    const res = await api.get(address + '/tags');
    dispatch({
      type: GET_TAGS,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: TAGS_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};
