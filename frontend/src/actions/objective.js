import api from '../utils/api';
import {
  GET_OBJECTIVES,
  OBJECTIVES_ERROR
} from './types';
import { host } from '../utils/host'
const address = 'http://' + host + ':4000';

export const getObjectives = () => async dispatch => {
  try {
    const res = await api.get(address + '/objectives');
    dispatch({
      type: GET_OBJECTIVES,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: OBJECTIVES_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};
