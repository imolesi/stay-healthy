import api from '../utils/api';
import {
  GET_DIETS,
  DIETS_ERROR
} from './types';
import { host } from '../utils/host'
const address = 'http://' + host + ':4000';

export const getDiets = () => async dispatch => {
  try {
    const res = await api.get(address + '/diets');
    dispatch({
      type: GET_DIETS,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: DIETS_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};
