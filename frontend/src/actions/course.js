import api from '../utils/api';
import { setAlert } from './alert';
import {
  GET_COURSES,
  COURSE_ERROR,
  DELETE_COURSE,
  ADD_COURSE,
  UPDATE_COURSE,
  GET_COURSE,
  SUBSCRIBE,
  UNSUBSCRIBE,
  ADD_COURSE_OBJECTIVE,
  REMOVE_COURSE_OBJECTIVE,
  CLEAR_COURSE
} from './types';
import { host } from '../utils/host'
const address = 'http://' + host + ':4000';

export const getCourses = () => async dispatch => {
  try {
    const res = await api.get(address + '/courses');
    dispatch({
      type: GET_COURSES,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: COURSE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const getCourse = id => async dispatch => {
  try {
    const res = await api.get(address + `/courses/${id}`);
    dispatch({
      type: GET_COURSE,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: COURSE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const deleteCourse = (id, history) => async dispatch => {
  try {
    history.push('/courses');
    await api.delete(address + `/courses/${id}`);
    dispatch({
      type: DELETE_COURSE,
      payload: id
    });
    dispatch(setAlert('Corso rimosso', 'success'));
  } catch(err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: COURSE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addCourse = (formData, history) => async dispatch => {
  const header = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  try {
    history.push('/manage-courses');
    const res = await api.post(address + '/courses', formData, header);
    dispatch({
      type: ADD_COURSE,
      payload: res.data
    });
    dispatch(setAlert('Corso creato', 'success'));
    if (res.data && res.data._id) {
      await api.get(address + `/subscriptions/course/${res.data._id}`);
    }
  } catch(err) {
    dispatch({
      type: COURSE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const updateCourse = (courseId, formData, history) => async dispatch => {
  const header = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  try {
    history.push('/manage-courses');
    const res = await api.post(address + `/courses/${courseId}`, formData, header);
    dispatch({
      type: UPDATE_COURSE,
      payload: { id: courseId, course: res.data }
    });
    dispatch(setAlert('Corso aggiornato', 'success'));
    await api.get(address + `/subscriptions/course/${courseId}`);
  } catch(err) {
    dispatch({
      type: COURSE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const subscribe = (courseId) => async dispatch => {
  const header = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  try {
    const res = await api.put(address + `/courses/subscribe/${courseId}`);
    dispatch({
      type: SUBSCRIBE,
      payload: { courseId, subscribers: res && res.data && res.data.subscribers ? res.data.subscribers : [] }
    });
    dispatch(setAlert('Iscritto aggiunto', 'success'));
    if (res && res.data) {
      await api.post(address + `/subscriptions/course-subscribe`, { user_id: res.data && res.data.subscribedId, course_id: courseId}, header);
    }
  } catch(err) {
    dispatch({
      type: COURSE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};


export const unsubscribe = (courseId) => async dispatch => {
  try {
    const res = await api.delete(address + `/courses/subscribe/${courseId}`);
    dispatch({
      type: UNSUBSCRIBE,
      payload: { courseId, subscribers: res.data }
    });
    dispatch(setAlert('Iscritto rimosso', 'success'));
  } catch(err) {
    dispatch({
      type: COURSE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addCourseObjective = (courseId, objectiveId) => async dispatch => {
  try {
    const res = await api.put(address + `/courses/objectives/${courseId}/${objectiveId}`);
    dispatch({
      type: ADD_COURSE_OBJECTIVE,
      payload: res.data
    });
    dispatch(setAlert('Obiettivo aggiunto', 'success'));
  } catch(err) {
    dispatch({
      type: COURSE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const removeCourseObjective = (courseId, objectiveId) => async dispatch => {
  try {
    await api.delete(address + `/courses/objectives/${courseId}/${objectiveId}`);
    dispatch({
      type: REMOVE_COURSE_OBJECTIVE,
      payload: objectiveId
    });
    dispatch(setAlert('Obiettivo rimosso', 'success'));
  } catch(err) {
    dispatch({
      type: COURSE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const clearCourse = () => async dispatch => {
  dispatch({ type: CLEAR_COURSE });
};
