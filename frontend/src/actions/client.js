import api from '../utils/api';
import { setAlert } from './alert';
import {
  GET_CLIENT,
  GET_CLIENTS,
  CLIENT_ERROR,
  UPDATE_CLIENT,
  CLEAR_CLIENT,
  ACCOUNT_DELETE
} from './types';
import { host } from '../utils/host'
const address = 'http://' + host + ':4000';

export const getClient = userId => async dispatch => {
  try {
    const res = await api.get(address + `/clients/${userId}`);
    dispatch({
      type: GET_CLIENT,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const getClients = () => async dispatch => {
  try {
    const res = await api.get(address + '/clients');
    dispatch({
      type: GET_CLIENTS,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const getCurrentClient = () => async dispatch => {
  try {
    const res = await api.get(address + '/clients/me');
    dispatch({
      type: GET_CLIENT,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const createClient = (formData, history, edit = false) => async dispatch => {
  try {
    const headers = {
      headers: {
        'Content-Type': 'application/json'
      }
    };
    const body = {
      client_height: formData.client_height,
      client_objectiveWeight: formData.client_objectiveWeight,
      client_interests: formData.client_interests,
      client_objectives: formData.client_objectives,
      client_diet: formData.client_diet
    };
    const res = await api.post(address + '/clients/', body, headers);
    dispatch({
      type: GET_CLIENT,
      payload: res.data
    });
    dispatch(setAlert(edit ? 'Profilo aggiornato' : 'Profilo creato', 'success'));
    if (!edit) {
      history.push('/dashboard');
      if (res.data && res.data.client_user) {
        await api.post(address + `/subscriptions/achievement/${res.data.client_user}`, {
          achievement_name: "Profilo completato",
          achievement_message: "Nuovo achievement! Profilo completato!"
        }, headers);
      }
    }
  } catch(err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addObjective = ( formData, history ) => async dispatch => {
  try {
    const headers = {
      headers: {
        'Content-Type': 'application/json'
      }
    };
    const body = {
      objective: formData.client_objective
    };
    const res = await api.put(address + '/clients/objective', body, headers);
    dispatch({
      type: UPDATE_CLIENT,
      payload: res.data
    });
    dispatch(setAlert('Obiettivi aggiornati', 'success'));
    history.push('/dashboard');
  } catch(err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addInterest = ( formData, history ) => async dispatch => {
  try {
    const headers = {
      headers: {
        'Content-Type': 'application/json'
      }
    };
    const body = {
      tag: formData.client_interest
    };
    const res = await api.put(address + '/clients/interest', body, headers);
    dispatch({
      type: UPDATE_CLIENT,
      payload: res.data
    });
    dispatch(setAlert('Interessi aggiornati', 'success'));
    history.push('/dashboard');
  } catch(err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addWeightProgress = ( formData, history ) => async dispatch => {
  try {
    const headers = {
      headers: {
        'Content-Type': 'application/json'
      }
    };
    const body = {
      weight: formData.client_currentWeight,
      date: new Date()
    };
    const res = await api.put(address + '/clients/weightProgress', body, headers);
    dispatch({
      type: UPDATE_CLIENT,
      payload: res.data
    });
    dispatch(setAlert('Progressi aggiornati', 'success'));
    history.push('/dashboard');
  } catch(err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const deleteInterest = id => async dispatch => {
  try {
    const res = await api.delete(address + `/clients/interest/${id}`);
    dispatch({
      type: UPDATE_CLIENT,
      payload: res.data
    });
    dispatch(setAlert('Interesse eliminato', 'success'));
  } catch(err) {
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const deleteObjective = id => async dispatch => {
  try {
    const res = await api.delete(address + `/clients/objective/${id}`);
    dispatch({
      type: UPDATE_CLIENT,
      payload: res.data
    });
    dispatch(setAlert('Obiettivo eliminato', 'success'));
  } catch(err) {
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const deleteWeightProgress = id => async dispatch => {
  try {
    const res = await api.delete(address + `/clients/weightProgress/${id}`);
    dispatch({
      type: UPDATE_CLIENT,
      payload: res.data
    });
    dispatch(setAlert('Progresso eliminato', 'success'));
  } catch(err) {
    dispatch({
      type: CLIENT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const deleteAccount = () => async dispatch => {
  if (window.confirm('Sei sicuro di voler eliminare il tuo profilo? Questa operazione non è reversibile')) {
    try {
      await api.delete(address + '/clients');
      dispatch({
        type: CLEAR_CLIENT
      });
      dispatch({
        type: ACCOUNT_DELETE
      });
      dispatch(setAlert('Il tuo account è stato eliminato permanentemente'));
    } catch(err) {
      dispatch({
        type: CLIENT_ERROR,
        payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
      });
    }
  }
};
