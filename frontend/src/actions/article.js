import api from '../utils/api';
import { setAlert } from './alert';
import {
  GET_ARTICLES,
  ARTICLE_ERROR,
  UPDATE_LIKES,
  DELETE_ARTICLE,
  ADD_ARTICLE,
  UPDATE_ARTICLE,
  GET_ARTICLE,
  ADD_COMMENT,
  REMOVE_COMMENT,
  ADD_ARTICLE_TAG,
  REMOVE_ARTICLE_TAG,
  CLEAR_ARTICLE
} from './types';
import { host } from '../utils/host'
const address = 'http://' + host + ':4000';

export const getArticles = () => async dispatch => {
  try {
    const res = await api.get(address + '/articles');
    dispatch({
      type: GET_ARTICLES,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const getArticle = id => async dispatch => {
  try {
    const res = await api.get(address + `/articles/${id}`);
    dispatch({
      type: GET_ARTICLE,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addLike = id => async dispatch => {
  try {
    const res = await api.put(address + `/articles/like/${id}`);
    dispatch({
      type: UPDATE_LIKES,
      payload: { id, likes: res.data }
    });
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const removeLike = id => async dispatch => {
  try {
    const res = await api.put(address + `/articles/unlike/${id}`);
    dispatch({
      type: UPDATE_LIKES,
      payload: { id, likes: res.data }
    });
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const deleteArticle = (id, history) => async dispatch => {
  try {
    history.push('/articles');
    await api.delete(address + `/articles/${id}`);
    dispatch({
      type: DELETE_ARTICLE,
      payload: id
    });
    dispatch(setAlert('Articolo rimosso', 'success'));
  } catch(err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addArticle = (formData, history) => async dispatch => {
  const header = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  try {
    history.push('/manage-articles');
    const res = await api.post(address + '/articles', formData, header);
    dispatch({
      type: ADD_ARTICLE,
      payload: res.data
    });
    dispatch(setAlert('Articolo creato', 'success'));
    if (res.data && res.data._id) {
      await api.get(address + `/subscriptions/article/${res.data._id}`);
    }
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const updateArticle = (articleId, formData, history) => async dispatch => {
  const header = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  try {
    history.push('/manage-articles');
    const res = await api.post(address + `/articles/${articleId}`, formData, header);
    dispatch({
      type: UPDATE_ARTICLE,
      payload: { id: articleId, article: res.data }
    });
    dispatch(setAlert('Articolo aggiornato', 'success'));
    await api.get(address + `/subscriptions/article/${articleId}`);
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addComment = (articleId, formData) => async dispatch => {
  const header = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  try {
    const res = await api.post(address + `/articles/comments/${articleId}`, formData, header);
    dispatch({
      type: ADD_COMMENT,
      payload: res.data
    });
    dispatch(setAlert('Commento aggiunto', 'success'));
    await api.get(address + `/subscriptions/comment/${articleId}`);

    if (res.data && res.data.comments && Array.isArray(res.data.comments) && res.data.user) {
      const res2 = await api.get(address + `/articles/comments/${res.data.user}`);
      if (res2.data && res2.data.numComments) {
        if ([1, 10, 100].includes(res2.data.numComments)) {
          let name = "";
          switch (res2.data.numComments) {
            case 1:
              name = "Commentatore in erba";
              break;
            case 10:
              name = "Commentatore provetto";
              break;
            case 100:
              name = "Commentatore esperto";
              break;
            default:
              name = "";
              break;
          }

          await api.post(address + `/subscriptions/achievement/${res.data.user}`, {
            achievement_name: name,
            achievement_message: `Nuovo achievement! Hai pubblicato ${res2.data.numComments} comment${res2.data.numComments === 1 ? "o" : "i"}!`
          }, header);
        }
      }
    }
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};


export const deleteComment = (articleId, commentId) => async dispatch => {
  try {
    await api.delete(address + `/articles/comments/${articleId}/${commentId}`);
    dispatch({
      type: REMOVE_COMMENT,
      payload: commentId
    });
    dispatch(setAlert('Commento rimosso', 'success'));
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addArticleTag = (articleId, tagId) => async dispatch => {
  try {
    const res = await api.put(address + `/articles/tag/${articleId}/${tagId}`);
    dispatch({
      type: ADD_ARTICLE_TAG,
      payload: res.data
    });
    dispatch(setAlert('Tag aggiunto', 'success'));
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const removeArticleTag = (articleId, tagId) => async dispatch => {
  try {
    await api.delete(address + `/articles/tag/${articleId}/${tagId}`);
    dispatch({
      type: REMOVE_ARTICLE_TAG,
      payload: tagId
    });
    dispatch(setAlert('Tag rimosso', 'success'));
  } catch(err) {
    dispatch({
      type: ARTICLE_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const clearArticle = () => async dispatch => {
  dispatch({ type: CLEAR_ARTICLE });
};
