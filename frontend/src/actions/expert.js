import api from '../utils/api';
import { setAlert } from './alert';
import {
  GET_EXPERT,
  GET_EXPERTS,
  EXPERT_ERROR,
  UPDATE_EXPERT,
  CLEAR_EXPERT,
  EXPERT_DELETE
} from './types';
import { host } from '../utils/host'
const address = 'http://' + host + ':4000';

export const getExpert = userId => async dispatch => {
  try {
    const res = await api.get(address + `/experts/${userId}`);
    dispatch({
      type: GET_EXPERT,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: EXPERT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const getExperts = () => async dispatch => {
  try {
    const res = await api.get(address + '/experts');
    dispatch({
      type: GET_EXPERTS,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: EXPERT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const getCurrentExpert = () => async dispatch => {
  try {
    const res = await api.get(address + '/experts/me');
    dispatch({
      type: GET_EXPERT,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: EXPERT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const createExpert = (formData, history, edit = false) => async dispatch => {
  try {
    const headers = {
      headers: {
        'Content-Type': 'application/json'
      }
    };
    const body = {
      expert_bio: formData.expert_bio,
      expert_knowledge: formData.expert_knowledge,
      expert_objectives: formData.expert_objectives
    };
    const res = await api.post(address + '/experts/', body, headers);
    dispatch({
      type: GET_EXPERT,
      payload: res.data
    });
    dispatch(setAlert(edit ? 'Profilo aggiornato' : 'Profilo creato', 'success'));
    if (!edit) {
      history.push('/dashboard');
    }
  } catch(err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: EXPERT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const addKnowledge = ( formData, history ) => async dispatch => {
  try {
    const headers = {
      headers: {
        'Content-Type': 'application/json'
      }
    };
    const body = {
      tag: formData.tag
    };
    const res = await api.put(address + '/experts/knowledge', body, headers);
    dispatch({
      type: UPDATE_EXPERT,
      payload: res.data
    });
    dispatch(setAlert('Esperienza aggiornata', 'success'));
    history.push('/dashboard');
  } catch(err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: EXPERT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const deleteKnowledge = id => async dispatch => {
  try {
    const res = await api.delete(address + `/experts/knowledge/${id}`);
    dispatch({
      type: UPDATE_EXPERT,
      payload: res.data
    });
    dispatch(setAlert('Esperienza eliminata', 'success'));
  } catch(err) {
    dispatch({
      type: EXPERT_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};

export const deleteExpert = () => async dispatch => {
  if (window.confirm('Sei sicuro di voler eliminare il tuo profilo? Questa operazione non è reversibile')) {
    try {
      const res = await api.delete(address + '/experts');
      dispatch({
        type: CLEAR_EXPERT
      });
      dispatch({
        type: EXPERT_DELETE,
        payload: res.data
      });
      dispatch(setAlert('Il tuo account è stato eliminato permanentemente'));
    } catch(err) {
      dispatch({
        type: EXPERT_ERROR,
        payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
      });
    }
  }
};
