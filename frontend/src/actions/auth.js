import api from '../utils/api';
import { setAlert } from './alert';
import serviceWorkerRegister from '../client';
import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_CLIENT,
  CLEAR_EXPERT
} from './types';
import { host } from '../utils/host'
const address = 'http://' + host + ':4000';

export const loadUser = () => async dispatch => {
  try {
    const res = await api.get(address + '/auth');
    if (res.data && res.data._id) {
      serviceWorkerRegister(res.data._id);
    }
    dispatch({
      type: USER_LOADED,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: AUTH_ERROR
    });
  }
};

export const register = ({
    user_name,
    user_lastname,
    user_gender,
    user_birthDate,
    user_username,
    user_email,
    user_password
  }) => async dispatch => {
  const headers = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  const body = JSON.stringify({
    user_name,
    user_lastname,
    user_gender,
    user_birthDate,
    user_username,
    user_email,
    user_password
  });
  try {
    const res = await api.post(address + '/users/register', body, headers);
    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data
    });
    dispatch(loadUser());
  } catch(err) {
    const errors = err.response.data.errors;
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: REGISTER_FAIL
    });
  }
};

export const login = ({
    user_username,
    user_password
  }) => async dispatch => {
  const headers = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  const body = JSON.stringify({
    user_username,
    user_password
  });
  try {
    const res = await api.post(address + '/auth', body, headers);
    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data
    });
    dispatch(loadUser());
  } catch(err) {
    if (err.response) {
      const errors = err.response.data.errors;
      if (errors) {
        errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
      }
    }
    dispatch({
      type: LOGIN_FAIL
    });
  }
};

export const logout = () => dispatch => {
  dispatch({ type: CLEAR_CLIENT });
  dispatch({ type: CLEAR_EXPERT });
  dispatch({ type: LOGOUT });
};
