import api from '../utils/api';
import {
  GET_ACHIEVEMENTS,
  ACHIEVEMENTS_ERROR
} from './types';
import { host } from '../utils/host'
const address = 'http://' + host + ':4000';

export const getAchievements = () => async dispatch => {
  try {
    const res = await api.get(address + '/achievements');
    dispatch({
      type: GET_ACHIEVEMENTS,
      payload: res.data
    });
  } catch(err) {
    dispatch({
      type: ACHIEVEMENTS_ERROR,
      payload: { msg: err.response && err.response.statusText, status: err.response && err.response.status }
    });
  }
};
